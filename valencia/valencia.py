import codecs
import pickle
from collections import defaultdict, deque
import xml.etree.ElementTree as et
import nbformat as nbf
import nbformat.v4 as nbv

from widgets.file import File, CSVFileImport, SaveData
from widgets.table import DataTable
from widgets.plot import ScatterPlot
from widgets.ml import LogisticRegression, kNN, Tree, NeuralNetwork
from widgets.predict import Predictions, TestandScore, ConfusionMatrix


def parse_node(node, properties):
    node_id = int(node.get('id'))
    name = node.get('name').replace(' ', '')
    node_format = properties.get('format')
    if node_format == 'pickle':
        d_properties = pickle.loads(codecs.decode(properties.text.encode(), 'base64'))
    elif node_format == 'literal':
        d_properties = properties.text
    return node_id, name, d_properties


def wfs_to_ipynb(wfs, ipynb=None):
    '''
    Converts Orange workflow file to IPython notebook.

    Args:
        wfs (str): Orange workflow file .wfs
        ipynb (str): Optional IPython notebook filepath

    Returns:
        str: The notebook filepath
    '''
    # parse xml
    tree = et.parse(wfs)
    nodes = tree.find('nodes').findall('node')
    links = tree.find('links').findall('link')
    node_properties = tree.find('node_properties').findall('properties')
    
    # create objects for every widget
    d_nodes = {}    
    for node, properties in zip(nodes, node_properties):
        node_id, name, properties = parse_node(node, properties)
        d_nodes[node_id] = globals()[name](node_id, properties)
    
    # update inputs and outputs of every widget
    l_links = list(links)
    sinks = set()
    while l_links:
        link = l_links.pop(0)
        source_id = int(link.get('source_node_id'))
        sink_id = int(link.get('sink_node_id'))
        source_channel = link.get('source_channel')
        sink_channel = link.get('sink_channel')
        sinks.add(sink_id)
        if source_data := d_nodes[source_id].outputs[source_channel]:
            d_nodes[sink_id].update_io(sink_channel, source_data)
        else:
            l_links.append(link)

    # create tree
    d_tree = defaultdict(list)
    d_n_sinks = defaultdict(int)
    for link in links:
        source_id = int(link.get('source_node_id'))
        sink_id = int(link.get('sink_node_id'))
        d_tree[source_id].append(link)
        d_n_sinks[sink_id] += 1
    
    # depth first tree traversal
    leaves = [node_id for node_id in d_nodes if node_id not in sinks]
    queue = deque(leaves)
    imports = set()
    cells = []
    while queue:
        source_id = queue.popleft()
        source = d_nodes[source_id]
        cells.append(source.get_code())
        imports |= source.imports
        for link in d_tree[source_id]:
            sink_id = int(link.get('sink_node_id'))
            source_channel = link.get('source_channel')
            sink_channel = link.get('sink_channel')
            d_n_sinks[sink_id] -= 1
            if d_n_sinks[sink_id] == 0:
                queue.appendleft(sink_id)
    
    # load imports
    cells.insert(0, '\n'.join(list(imports)))
    
    # create the notebook
    if ipynb is None:
        ipynb = wfs.rstrip('wfs') + 'ipynb'
    nb = nbv.new_notebook()
    nb['cells'] = [nbv.new_code_cell(cell) for cell in cells if cell]
    nb['cells'] += 3*[nbv.new_code_cell([])]
    nbf.write(nb, open(ipynb, 'w'))
    return ipynb
