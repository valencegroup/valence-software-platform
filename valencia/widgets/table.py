from .base import Widget


class DataTable(Widget):
    def __init__(self, *args):
        inputs = {'Data': ''}
        outputs = {'Data': '', 'Selected Data': ''}
        imports = set()
        super().__init__(*args, inputs, outputs, imports)
    
    def update_io(self, channel, data):
        self.inputs[channel] = data
        self.outputs['Data'] = self.inputs['Data']
        self.outputs['Selected Data'] = self.inputs['Data']
    
    def get_code(self):
        var = self.inputs['Data']
        code = [
            f'{var}',
        ]
        return '\n'.join(code)
