from .base import Widget


class LogisticRegression(Widget):
    def __init__(self, *args):
        inputs = {'Data': '', 'Preprocessor': ''}
        outputs = {'Learner': '', 'Model': '', 'Coefficients': ''}
        imports = {'from sklearn.linear_model import LogisticRegression'}
        super().__init__(*args, inputs, outputs, imports)
        
        self.outputs['Model'] = 'clf_log_reg'
        self.outputs['Learner'] = 'LogisticRegression()'
    
    def update_io(self, channel, data):
        self.inputs[channel] = data
    
    def get_code(self):
        var_model = self.outputs['Model']
        var_data = self.inputs['Data']
        code = []
        if var_data:
            code += [
                f'x = {var_data}.iloc[:, :-1]',
                f'y = {var_data}.iloc[:, -1]',
                f'{var_model} = LogisticRegression().fit(x, y)',
            ]
        return '\n'.join(code)


class kNN(Widget):
    def __init__(self, *args):
        inputs = {'Data': '', 'Preprocessor': ''}
        outputs = {'Learner': '', 'Model': '', 'Coefficients': ''}
        imports = {'from sklearn.neighbors import KNeighborsClassifier'}
        super().__init__(*args, inputs, outputs, imports)
        
        self.outputs['Model'] = 'clf_knn'
        self.outputs['Learner'] = 'KNeighborsClassifier()'
    
    def update_io(self, channel, data):
        self.inputs[channel] = data
    
    def get_code(self):
        var_model = self.outputs['Model']
        var_data = self.inputs['Data']
        code = []
        if var_data:
            code += [
                f'x = {var_data}.iloc[:, :-1]',
                f'y = {var_data}.iloc[:, -1]',
                f'{var_model} = KNeighborsClassifier().fit(x, y)',
            ]
        return '\n'.join(code)


class Tree(Widget):
    def __init__(self, *args):
        inputs = {'Data': '', 'Preprocessor': ''}
        outputs = {'Learner': '', 'Model': '', 'Coefficients': ''}
        imports = {'from sklearn.tree import DecisionTreeClassifier'}
        super().__init__(*args, inputs, outputs, imports)
        
        self.outputs['Model'] = 'clf_tree'
        self.outputs['Learner'] = 'DecisionTreeClassifier()'
    
    def update_io(self, channel, data):
        self.inputs[channel] = data
    
    def get_code(self):
        var_model = self.outputs['Model']
        var_data = self.inputs['Data']
        code = []
        if var_data:
            code += [
                f'x = {var_data}.iloc[:, :-1]',
                f'y = {var_data}.iloc[:, -1]',
                f'{var_model} = DecisionTreeClassifier().fit(x, y)',
            ]
        return '\n'.join(code)


class NeuralNetwork(Widget):
    def __init__(self, *args):
        inputs = {'Data': '', 'Preprocessor': ''}
        outputs = {'Learner': '', 'Model': '', 'Coefficients': ''}
        imports = {'from sklearn.neural_network import MLPClassifier'}
        super().__init__(*args, inputs, outputs, imports)
        
        self.outputs['Model'] = 'clf_nn'
        self.outputs['Learner'] = 'MLPClassifier()'
    
    def update_io(self, channel, data):
        self.inputs[channel] = data
    
    def get_code(self):
        var_model = self.outputs['Model']
        var_data = self.inputs['Data']
        code = []
        if var_data:
            code += [
                f'x = {var_data}.iloc[:, :-1]',
                f'y = {var_data}.iloc[:, -1]',
                f'{var_model} = MLPClassifier().fit(x, y)',
            ]
        return '\n'.join(code)
