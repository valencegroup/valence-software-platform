class Widget:
    def __init__(self, node_id, properties, inputs, outputs, imports):
        self.node_id = node_id
        self.properties = properties
        self.inputs = inputs
        self.outputs = outputs
        self.imports = imports
