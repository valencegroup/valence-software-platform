import pathlib
import pandas as pd
from .base import Widget


class File(Widget):
    def __init__(self, *args):
        inputs = {}
        outputs = {'Data': ''}
        imports = {'import pandas as pd'}
        super().__init__(*args, inputs, outputs, imports)
        
        path = self.properties['recent_paths'][0]
        self.path_orange = pathlib.Path(getattr(path, 'abspath'))
        self.path = pathlib.Path(f'{self.path_orange.stem}.csv')
        # FIXME: tell the difference between .csv and orange .tab
        # TODO: check if the file exists
        if self.path_orange.suffix == '.tab':
            self.create_csv_from_orange_tab()
        else:
            pd.read_csv(self.path_orange).to_csvv(self.path, index=None)
        self.outputs['Data'] = f'df_{self.path.stem}'
    
    def create_csv_from_orange_tab(self):
        """ Converts orange .tab to .csv """
        # TODO: don't skip the meta columns
        df = pd.read_csv(self.path_orange, sep='\t')
        cols_y = df.loc[1][df.loc[1] == 'class'].index
        cols_meta = df.loc[1][df.loc[1] == 'meta'].index
        df = df.drop([0, 1])
        df_x = df[[col for col in df.columns if col not in cols_y and col not in cols_meta]]
        df_y = df[[col for col in df.columns if col in cols_y]]
        df = pd.concat([df_x, df_y], axis=1)
        df.to_csv(self.path, index=None)
    
    def get_code(self):
        var_df = self.outputs['Data']
        code = [
            f"{var_df} = pd.read_csv('{self.path}')",
        ]
        return '\n'.join(code)


class CSVFileImport(Widget):
    def __init__(self, *args):
        inputs = {}
        outputs = {'Data': ''}
        imports = {'import pandas as pd'}
        super().__init__(*args, inputs, outputs, imports)

        self.outputs['Data'] = 'df'

    def get_code(self):
        var_df = self.outputs['Data']
        code = [
            f"{var_df} = pd.read_csv('enter_filepath_here.csv')",
        ]
        return '\n'.join(code)


class SaveData(Widget):
    def __init__(self, *args):
        inputs = {'Data': ''}
        outputs = {}
        imports = {'import pandas as pd'}
        super().__init__(*args, inputs, outputs, imports)

        self.outputs['Data'] = 'df'

    def update_io(self, channel, data):
        self.inputs[channel] = data

    def get_code(self):
        var_data = self.inputs['Data']
        code = [f"{var_data}.to_csv('enter_filepath_here.csv')"]
        return '\n'.join(code)
