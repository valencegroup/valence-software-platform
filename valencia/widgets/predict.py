from .base import Widget


class Predictions(Widget):
    def __init__(self, *args):
        inputs = {'Data': '', 'Predictors': []}
        outputs = {'Predictions': '', 'Evaluation Results': []}
        imports = {
            'import numpy as np',
            'import pandas as pd',
        }
        super().__init__(*args, inputs, outputs, imports)

    def update_io(self, channel, data):
        if channel == 'Predictors':
            self.inputs[channel].append(data)
            index = len(self.outputs['Evaluation Results'])
            self.outputs['Evaluation Results'].extend(['y', f'y_{index}_predicted'])
        else:
            self.inputs[channel] = data
            self.outputs['Predictions'] = self.inputs['Data'] + '_predicted'

    def get_code(self):
        var_data = self.inputs['Data']
        var_predictions  = self.outputs['Predictions']
        code = []
        for index, var_predictor in enumerate(self.inputs['Predictors']):
            code += [
                f'y_{index}_predicted = {var_predictor}.predict({var_data})',
            ]
        n = len(self.inputs['Predictors'])
        predictions = ', '.join([f'y_{x}_predicted' for x in range(n)])
        if n == 1:
            code += [f'{var_predictions} = pd.DataFrame({predictions})']
        elif n > 1:
            code += [f'{var_predictions} = pd.DataFrame(np.vstack(({predictions})).T)']
        return '\n'.join(code)


class TestandScore(Widget):
    def __init__(self, *args):
        inputs = {'Data': '', 'Test Data': '', 'Learner': [], 'Preprocessor': ''}
        outputs = {'Predictions': '', 'Evaluation Results': []}
        imports = {
            'import numpy as np',
            'import pandas as pd',
            'from sklearn.model_selection import cross_val_score',
            'from sklearn.model_selection import cross_val_predict',
        }
        super().__init__(*args, inputs, outputs, imports)

    def update_io(self, channel, data):
        if channel == 'Learner':
            self.inputs[channel].append(data)
            index = len(self.outputs['Evaluation Results'])
            self.outputs['Evaluation Results'].extend(['y', f'y_{index}_predicted'])
        else:
            self.inputs[channel] = data
            self.outputs['Predictions'] = self.inputs['Data'] + '_predicted'

    def get_code(self):
        var_data = self.inputs['Data']
        var_predictions  = self.outputs['Predictions']
        code = []
        if var_data:
            code += [
                f'x = {var_data}.iloc[:, :-1]',
                f'y = {var_data}.iloc[:, -1]',
            ]
        predictions = []
        for index, var_learner in enumerate(self.inputs['Learner']):
            predictions.append(f'y_{index}_predicted')
            code += [
                f'clf = {var_learner}',
                f'y_{index}_score = cross_val_score(clf, x, y)',
                f'y_{index}_predicted = cross_val_predict(clf, x, y)',
            ]
        n = len(self.inputs['Learner'])
        predictions = ', '.join([f'y_{x}_predicted' for x in range(n)])
        if n == 1:
            code += [f'{var_predictions} = pd.DataFrame({predictions})']
        elif n > 1:
            code += [f'{var_predictions} = pd.DataFrame(np.vstack(({predictions})).T)']
        return '\n'.join(code)


class ConfusionMatrix(Widget):
    def __init__(self, *args):
        inputs = {'Evaluation Results': []}
        outputs = {'Data': '', 'Selected Data': ''}
        imports = {'from sklearn.metrics import confusion_matrix'}
        super().__init__(*args, inputs, outputs, imports)

        self.outputs['Data'] = 'y'
        self.outputs['Selected Data'] = 'y'

    def update_io(self, channel, data):
        self.inputs[channel].append(data)

    def get_code(self):
        l_results = self.inputs['Evaluation Results']
        code = []
        for result in l_results:
            y, y_prediction = result
            code += [f'confusion_matrix({y}, {y_prediction})']
        return '\n'.join(code)
