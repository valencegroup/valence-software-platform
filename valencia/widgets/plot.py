from .base import Widget


class ScatterPlot(Widget):
    def __init__(self, *args):
        inputs = {'Data': '', 'Data Subset': '', 'Features': ''}
        outputs = {'Data': '', 'Selected Data': '', 'Features': ''}
        imports = {'import plotly.express as px'}
        super().__init__(*args, inputs, outputs, imports)
    
    def update_io(self, channel, data):
        self.inputs[channel] = data
        self.outputs['Data'] = self.inputs['Data']
        self.outputs['Selected Data'] = self.inputs['Data']
    
    def get_code(self):
        var_data = self.inputs['Data']
        code = [
            f'x, y = {var_data}.columns[:2]',
            f'px.scatter({var_data}, x=x, y=y)',
        ]
        return '\n'.join(code)
