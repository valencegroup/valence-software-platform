# VALENCE Software Platform

This project hosts the source code for Software Platform developed for the [VALENCE Project: Advancing Machine Learning and Data Science in Vocation High School Education](http://www.valence-project.eu/).

The platform includes a range of applications designed to teach students about different machine learning models. 
It is meant to augment the [VALENCE Machine Learning Curriculum](https://github.com/VALENCEML/eBOOK).

## Valencia
Valencia is the codename for the development of interactive applications for the Orange ecosystem, and code that will translate Orange widgets to Jupyter code cells. 

Trivia: Valencia is also a type of orange.

## Valetudo
Valetudo is the codename for the development of interactive applications for the jupyter ecosystem.

Trivia: Valetudo is also a moon of Jupiter.

### Installation steps

- Create a conda environment.
```sh
conda create -n valence
```
- Activate the environment.
```sh
conda activate valence
```
- Install the required packages.
```sh
conda install -yc conda-forge jupyterlab nodejs numpy scipy pandas scikit-learn ipywidgets bqplot python-graphviz
```
### Run
To test the interactive applications, open the `Valetudo.ipynb` notebook.
```sh
jupyter lab
```
