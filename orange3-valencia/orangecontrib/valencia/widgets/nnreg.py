import numpy as np
import pandas as pd
from sklearn import neural_network
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning

import pyqtgraph as pg
from AnyQt.QtCore import Qt, pyqtSignal as Signal, QPointF
from AnyQt.QtGui import QColor

from Orange.data import ContinuousVariable, DiscreteVariable, Domain, Table
from Orange.widgets import gui, settings
from Orange.widgets.settings import Setting
from Orange.widgets.utils.itemmodels import DomainModel
from Orange.widgets.widget import OWWidget, Input, Output, Msg
from Orange.widgets.utils.colorpalettes import DefaultRGBColors


class MLPRegressor:
    @ignore_warnings(category=ConvergenceWarning)
    def __init__(
            self,
            df,
            hiddens=(10,),
            alpha=0.01,
            learn=0.01,
            max_iter=50,
            ):
        self.reg = neural_network.MLPRegressor(
            hidden_layer_sizes=hiddens,
            alpha=alpha,
            learning_rate_init=learn,
            max_iter=max_iter,
            activation="tanh",
            solver="sgd",
            tol=1e-9,
            random_state=42,
            )
        self.reg.fit(df[['x']], df['y'])

    @ignore_warnings(category=ConvergenceWarning)
    def fit(self, df):
        self.reg.fit(df[['x']], df['y'])

    def predict(self, x):
        preds = self.reg.predict(x)
        return preds

    def set_params(
            self,
            hiddens=None,
            alpha=None,
            learn=None,
            max_iter=None,
            ):
        if hiddens is not None:
            self.reg.set_params(hidden_layer_sizes=hiddens)
        if alpha is not None:
            self.reg.set_params(alpha=alpha)
        if learn is not None:
            self.reg.set_params(learning_rate_init=learn)
        if max_iter is not None:
            self.reg.set_params(max_iter=max_iter)


class NeuralNetworkRegressionPlotWidget(pg.PlotWidget):
    on_drag_points = Signal(int, float, float)
    on_hover_points = Signal(int, float, float)
    on_drag_end_points = Signal(int, float, float)
    on_click_points = Signal(int)
    on_background_click = Signal(float, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sr_points = None
        self.moved = False
        self.mouse_down = False
        self.index = None

    def set_sr_points(self, sr_points):
        self.sr_points = sr_points

    def mousePressEvent(self, ev):
        no_data = self.sr_points is None
        if ev.button() != Qt.LeftButton or no_data:
            ev.ignore()
            return
        ev.accept()
        self.mouse_down = True
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            self.index = self.sr_points.points().tolist().index(pts_points[0])

    def mouseReleaseEvent(self, ev):
        if ev.button() != Qt.LeftButton:
            ev.ignore()
            return
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        x, y = pos.x(), pos.y()
        if self.index is None:
            if not self.moved:
                self.on_background_click.emit(x, y)
        else:
            if self.moved:
                self.on_drag_end_points.emit(self.index, x, y)
            else:
                self.on_click_points.emit(self.index)
        self.index = None
        self.mouse_down = False
        self.moved = False

    def mouseMoveEvent(self, ev):
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            index = self.sr_points.points().tolist().index(pts_points[0])
            self.on_hover_points.emit(index, pos.x(), pos.y())
        if self.mouse_down and self.index is not None:
            self.moved = True
            self.on_drag_points.emit(self.index, pos.x(), pos.y())


class ValenciaNeuralNetworkRegression(OWWidget):
    name = 'NN Regression'
    description = 'Interactive application for neural network regression.'
    icon = 'icons/nnreg.svg'
    priority = 100
    keywords = ['neural', 'regression', 'interactive']
    want_main_area = True
    want_control_area = True
    resizing_enabled = True

    cs_x = settings.ContextSetting(None)
    cs_y = settings.ContextSetting(None)
    cs_h1 = settings.ContextSetting(5)
    cs_h2 = settings.ContextSetting(0)
    cs_h3 = settings.ContextSetting(0)
    cs_max_iter = settings.ContextSetting(100)
    cs_learn = settings.ContextSetting(0.1)
    cs_alpha = settings.ContextSetting(0.1)
    cs_autorange = settings.ContextSetting(True)
    cs_scale_axes = settings.ContextSetting(False)

    class Inputs:
        data = Input("Data", Table)

    class Outputs:
        data = Output("Data", Table, default=True)

    class Error(OWWidget.Error):
        n_features = Msg('Data must contain at least two numeric variables.')
        n_class = Msg('Data must contain at least one categorical variable.')
        min_rows = Msg('Not enough data rows.')

    def __init__(self):
        super().__init__()
        self.rng = np.random.default_rng()
        self.colors = DefaultRGBColors.qcolors[np.arange(10)]
        self.max_n_classes = len(self.colors)
        self.pens = np.array([pg.mkPen(x.darker(120)) for x in self.colors])
        self.brushes = np.array([pg.mkBrush(x) for x in self.colors])
        self.create_app_controls()
        self.add_marks()
        self.bind_signals()
        self.set_data(None)

    def create_app_controls(self):
        self.controlArea.layout().setAlignment(Qt.AlignTop)
        self.dm_features = DomainModel(
            valid_types=(ContinuousVariable, ), order=DomainModel.MIXED)
        gui.comboBox(
            self.controlArea, self, value='cs_x', model=self.dm_features,
            callback=self.ck_bt_reset, label='X:')
        gui.comboBox(
            self.controlArea, self, value='cs_y', model=self.dm_features,
            callback=self.ck_bt_reset, label='Y:')
        self.bt_reset = gui.button(
            self.controlArea, self, 'Reset', callback=self.ck_bt_reset)
        self.sl_h1 = gui.spin(
            self.controlArea, self, 'cs_h1', minv=1, maxv=10,
            label='HL 1:', callback=self.cg_sl_h)
        self.sl_h2 = gui.spin(
            self.controlArea, self, 'cs_h2', minv=0, maxv=10,
            label='HL 2:', callback=self.cg_sl_h)
        self.sl_h3 = gui.spin(
            self.controlArea, self, 'cs_h3', minv=0, maxv=10,
            label='HL 3:', callback=self.cg_sl_h)
        self.sl_max_iter = gui.spin(
            self.controlArea, self, 'cs_max_iter', minv=1, maxv=5000, step=100,
            label='Max iterations:', callback=self.cg_sl_max_iter)
        self.sl_learn = gui.doubleSpin(
            self.controlArea, self, 'cs_learn', minv=0.01, maxv=1, step=0.01,
            label='Learning rate:', callback=self.cg_sl_learn)
        self.sl_alpha = gui.doubleSpin(
            self.controlArea, self, 'cs_alpha', minv=0.001, maxv=1, step=0.001,
            label='Regularization:', callback=self.cg_sl_alpha)
        self.ch_autorange = gui.checkBox(
            self.controlArea, self, 'cs_autorange', label='Autorange:',
            callback=self.cg_ch_autorange)
        self.ch_scale_axes = gui.checkBox(
            self.controlArea, self, 'cs_scale_axes', label='Scale Axes:',
            callback=self.cg_ch_scale_axes)

    def add_marks(self):
        self.fig = NeuralNetworkRegressionPlotWidget()
        self.fig.disableAutoRange()
        self.mainArea.layout().addWidget(self.fig)
        self.mlp_regression = pg.PlotCurveItem()
        self.fig.addItem(self.mlp_regression)
        self.mlp_regression.setPen(self.pens[2])
        self.sr_points = pg.ScatterPlotItem(symbol='o', size=8)
        self.fig.addItem(self.sr_points)
        self.fig.set_sr_points(self.sr_points)

    def bind_signals(self):
        self.fig.on_drag_points.connect(self.dr_sr_points)
        self.fig.on_drag_end_points.connect(self.dre_sr_points)
        self.fig.on_click_points.connect(self.ck_sr_points)
        self.fig.on_background_click.connect(self.ck_sr_points_background)

    @Inputs.data
    def set_data(self, data):
        self.Error.clear()
        self.sr_points.setData([], [])
        self.mlp_regression.setData([], [])
        self.data_original = data
        if data is None:
            self.source_df = 'internal'
            self.dm_features.set_domain(None)
        else:
            self.source_df = 'external'
            self.dm_features.set_domain(data.domain)
            if len(self.dm_features) < 2:
                self.Error.n_features()
                self.dm_features.set_domain(None)
                return
            self.cs_x, self.cs_y = self.dm_features[:2]
            self.reload_df_from_orange_table()
            self.check_arg_df_min_rows(self.df, 2)
        self.ck_bt_reset()

    def reload_df_from_orange_table(self):
        df_x, df_y = self.data_original.X_df, self.data_original.Y_df
        self.df = pd.concat([df_x, df_y], axis=1)
        self.df.reset_index(drop=True, inplace=True)

    def ck_bt_reset(self):
        column_names = ['x', 'y']
        if self.source_df == 'internal':
            self.x, self.y = column_names
            data = self.rng.standard_normal(size=(3, 2))
            self.df = pd.DataFrame(data, columns=column_names)
        elif self.source_df == 'external':
            self.reload_df_from_orange_table()
            self.x, self.y = self.cs_x.name, self.cs_y.name
            self.df = self.df[[self.x, self.y]]
            self.df.columns = column_names
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes()
        self.project_mlp_regression()

    def update_after_df(self):
        self.validate_df()

    def validate_df(self):
        self.Error.clear()
        self.valid = self.validate_df_runtime()

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            self.Error.min_rows()
            return False
        return True

    def check_arg_df_min_rows(self, df, n):
        if df.shape[0] < n:
            self.Error.min_rows()

    def reset_reg(self):
        if not self.valid:
            return
        hiddens = [self.cs_h1, self.cs_h2, self.cs_h3]
        hiddens = [h for h in hiddens if h > 0]
        self.reg = MLPRegressor(
            self.df,
            hiddens=hiddens,
            alpha=self.cs_alpha,
            learn=self.cs_learn,
            max_iter=self.cs_max_iter
        )

    def update_sr_points(self):
        x, y = self.df.x.astype(float), self.df.y.astype(float)
        self.sr_points.setData(x, y)

    def get_data_limits(self):
        x_min, x_max = self.df.x.min(), self.df.x.max()
        y_min, y_max = self.df.y.min(), self.df.y.max()
        return x_min, x_max, y_min, y_max

    def project_mlp_regression(self):
        if not self.valid:
            return
        (x_min, x_max), (y_min, y_max) = self.fig.viewRange()
        x = np.linspace(x_min, x_max, 300)
        y = self.reg.predict(pd.DataFrame({'x': x}))
        self.mlp_regression.setData(x, y)

    def cg_sl_h(self):
        hiddens = [self.cs_h1, self.cs_h2, self.cs_h3]
        hiddens = [h for h in hiddens if h > 0]
        self.reg.set_params(hiddens=hiddens)
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def cg_sl_max_iter(self):
        self.reg.set_params(max_iter=self.cs_max_iter)
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def cg_sl_learn(self):
        self.reg.set_params(learn=self.cs_max_iter)
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def cg_sl_alpha(self):
        self.reg.set_params(alpha=self.cs_max_iter)
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def cg_ch_autorange(self):
        self.update_axes()

    def cg_ch_scale_axes(self):
        self.fig.setAspectLocked(self.cs_scale_axes)

    def dr_sr_points(self, index, x, y):
        self.df.loc[index, ['x', 'y']] = x, y
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.project_mlp_regression()

    def dre_sr_points(self, index, x, y):
        self.update_axes()

    def ck_sr_points(self, index):
        self.df = self.df.drop(self.df.index[index])
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes()
        self.project_mlp_regression()

    def ck_sr_points_background(self, x, y):
        new_point = x, y
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.project_mlp_regression()

    def update_axes(self):
        if self.cs_autorange:
            self.fig.autoRange(items=[self.sr_points])


if __name__ == "__main__":
    from Orange.widgets.utils.widgetpreview import WidgetPreview
    WidgetPreview(ValenciaNeuralNetworkRegression).run()
