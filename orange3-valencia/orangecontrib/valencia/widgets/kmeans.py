import numpy as np
import pandas as pd
from sklearn import cluster

import pyqtgraph as pg
from AnyQt.QtCore import Qt, pyqtSignal as Signal, QPointF
from AnyQt.QtGui import QColor

from Orange.data import ContinuousVariable, Domain, Table
from Orange.widgets import gui, settings
from Orange.widgets.settings import Setting
from Orange.widgets.utils.itemmodels import DomainModel
from Orange.widgets.widget import OWWidget, Input, Output, Msg
from Orange.widgets.utils.colorpalettes import DefaultRGBColors


class KMeans:
    def __init__(self, df, n_centroids, n_steps):
        self.clf = cluster.KMeans(
            n_clusters=n_centroids, n_init=1, max_iter=n_steps)
        self.fit(df)

    def fit(self, df):
        self.clf.fit(df[['f1', 'f2']])

    def get_centroids(self):
        return self.clf.cluster_centers_.T

    def set_centroids(self, centroids):
        self.clf.cluster_centers_ = centroids.copy(order='C')

    def set_params(self, centroids, max_iter):
        self.clf.set_params(
            n_clusters=len(centroids), init=centroids, max_iter=max_iter)

    def predict(self, df):
        if df.shape[0] == 0:
            return []
        return self.clf.predict(df[['f1', 'f2']])


class KMeansPlotWidget(pg.PlotWidget):
    on_drag_points = Signal(int, float, float)
    on_drag_centroids = Signal(int, float, float)
    on_drag_end_points = Signal(int, float, float)
    on_drag_end_centroids = Signal(int, float, float)
    on_click_points = Signal(int)
    on_click_centroids = Signal(int)
    on_background_click = Signal(float, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sr = None
        self.sr_points = None
        self.sr_centroids = None
        self.moved = False
        self.mouse_down = False
        self.index = None

    def set_sr_points(self, sr_points):
        self.sr_points = sr_points

    def set_sr_centroids(self, sr_centroids):
        self.sr_centroids = sr_centroids

    def mousePressEvent(self, ev):
        no_data = self.sr_points is None or self.sr_centroids is None
        if ev.button() != Qt.LeftButton or no_data:
            ev.ignore()
            return
        ev.accept()
        self.mouse_down = True
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        pts_centroids = self.sr_centroids.pointsAt(pos)
        if len(pts_centroids) > 0:
            self.sr = 'centroids'
            self.index = self.sr_centroids.points().tolist().index(
                pts_centroids[0])
        elif len(pts_points) > 0:
            self.sr = 'points'
            self.index = self.sr_points.points().tolist().index(pts_points[0])

    def mouseReleaseEvent(self, ev):
        if ev.button() != Qt.LeftButton:
            ev.ignore()
            return
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        x, y = pos.x(), pos.y()
        if self.index is None:
            if not self.moved:
                self.on_background_click.emit(x, y)
        else:
            if self.moved and self.sr == 'points':
                self.on_drag_end_points.emit(self.index, x, y)
            elif self.moved and self.sr == 'centroids':
                self.on_drag_end_centroids.emit(self.index, x, y)
            elif not self.moved and self.sr == 'points':
                self.on_click_points.emit(self.index)
            elif not self.moved and self.sr == 'centroids':
                self.on_click_centroids.emit(self.index)
        self.index = None
        self.mouse_down = False
        self.moved = False
        self.sr = None

    def mouseMoveEvent(self, ev):
        if not self.mouse_down:
            ev.ignore()
            return
        ev.accept()
        if self.index is not None:
            self.moved = True
            pos = self.plotItem.mapToView(QPointF(ev.pos()))
            if self.sr == 'points':
                self.on_drag_points.emit(self.index, pos.x(), pos.y())
            elif self.sr == 'centroids':
                self.on_drag_centroids.emit(self.index, pos.x(), pos.y())


class ValenciaKMeans(OWWidget):
    name = 'K-Means'
    description = 'Interactive application for K-Means.'
    icon = 'icons/kmeans.svg'
    priority = 100
    keywords = ['k-means', 'interactive']
    want_main_area = True
    want_control_area = True
    resizing_enabled = True

    cs_f1 = settings.ContextSetting(None)
    cs_f2 = settings.ContextSetting(None)
    cs_k = settings.ContextSetting(3)
    cs_autorange = settings.ContextSetting(True)
    cs_scale_axes = settings.ContextSetting(False)

    class Inputs:
        data = Input("Data", Table)

    class Outputs:
        data = Output("Data", Table, default=True)

    class Error(OWWidget.Error):
        n_features = Msg('Data must contain at least two numeric variables.')
        min_rows = Msg('Not enough data rows.')

    def __init__(self):
        super().__init__()
        self.rng = np.random.default_rng()
        self.colors = DefaultRGBColors.qcolors[np.arange(10)]
        self.max_n_classes = len(self.colors)
        self.pens = np.array([pg.mkPen(x.darker(120)) for x in self.colors])
        self.brushes = np.array([pg.mkBrush(x) for x in self.colors])
        self.create_app_controls()
        self.add_marks()
        self.bind_signals()
        self.set_data(None)

    def create_app_controls(self):
        self.controlArea.layout().setAlignment(Qt.AlignTop)
        self.dm_features = DomainModel(
            valid_types=(ContinuousVariable, ), order=DomainModel.MIXED)
        gui.comboBox(
            self.controlArea, self, value='cs_f1', model=self.dm_features,
            callback=self.ck_bt_reset, label='Feature 1:')
        gui.comboBox(
            self.controlArea, self, value='cs_f2', model=self.dm_features,
            callback=self.ck_bt_reset, label='Feature 2:')
        self.bt_reset = gui.button(
            self.controlArea, self, 'Reset', callback=self.ck_bt_reset)
        self.sl_k = gui.spin(
            self.controlArea, self, 'cs_k', minv=1, maxv=10, label='K:',
            callback=self.cg_sl_k)
        self.bt_run_at_once = gui.button(
            self.controlArea, self, 'Run At Once',
            callback=self.ck_bt_run_at_once)
        self.bt_step = gui.button(
            self.controlArea, self, 'Step', callback=self.ck_bt_step)
        self.ch_autorange = gui.checkBox(
            self.controlArea, self, 'cs_autorange', label='Autorange:',
            callback=self.cg_ch_autorange)
        self.ch_scale_axes = gui.checkBox(
            self.controlArea, self, 'cs_scale_axes', label='Scale Axes:',
            callback=self.cg_ch_scale_axes)

    def add_marks(self):
        self.fig = KMeansPlotWidget()
        self.mainArea.layout().addWidget(self.fig)
        self.sr_points = pg.ScatterPlotItem(symbol='o', size=8)
        self.fig.addItem(self.sr_points)
        self.fig.set_sr_points(self.sr_points)
        self.sr_centroids = pg.ScatterPlotItem(symbol='o', size=16)
        self.fig.addItem(self.sr_centroids)
        self.fig.set_sr_centroids(self.sr_centroids)

    def bind_signals(self):
        self.fig.on_drag_points.connect(self.dr_sr_points)
        self.fig.on_drag_end_points.connect(self.dre_sr_points)
        self.fig.on_drag_centroids.connect(self.dr_sr_centroids)
        self.fig.on_drag_end_centroids.connect(self.dre_sr_centroids)
        self.fig.on_click_points.connect(self.ck_sr_points)
        self.fig.on_background_click.connect(self.ck_sr_points_background)

    @Inputs.data
    def set_data(self, data):
        self.Error.clear()
        self.sr_points.setData([], [])
        self.sr_centroids.setData([], [])
        self.data_original = data
        if data is None:
            self.source_df = 'internal'
            self.dm_features.set_domain(None)
        else:
            self.source_df = 'external'
            self.dm_features.set_domain(data.domain)
            if len(self.dm_features) < 2:
                self.Error.n_features()
                self.dm_features.set_domain(None)
                return
            self.cs_f1, self.cs_f2 = self.dm_features[:2]
            self.reload_df_from_orange_table()
            self.check_arg_df_min_rows(self.df, 1)
        self.ck_bt_reset()

    def reload_df_from_orange_table(self):
        df_x, df_y = self.data_original.X_df, self.data_original.Y_df
        self.df = pd.concat([df_x, df_y], axis=1)
        self.df.reset_index(drop=True, inplace=True)

    def ck_bt_reset(self):
        column_names = ['f1', 'f2']
        if self.source_df == 'internal':
            data = self.rng.standard_normal(size=(5, 2))
            self.df = pd.DataFrame(data, columns=column_names)
        elif self.source_df == 'external':
            self.reload_df_from_orange_table()
            self.f1, self.f2 = self.cs_f1.name, self.cs_f2.name
            self.df = self.df[[self.f1, self.f2]]
            self.df.columns = column_names
        self.update_after_df()
        self.reset_clf()
        self.init_centroids()
        self.update_sr_points()
        self.update_sr_centroids()

    def update_after_df(self):
        self.validate_df()
        self.sl_k.setMaximum(min(self.max_n_classes, max(1, self.df.shape[0])))

    def validate_df(self):
        self.Error.clear()
        self.valid = self.validate_df_runtime()

    def validate_df_runtime(self):
        if self.df.shape[0] < 1:
            self.Error.min_rows()
            return False
        return True

    def check_arg_df_min_rows(self, df, n):
        if df.shape[0] < n:
            self.Error.min_rows()

    def reset_clf(self):
        if not self.valid:
            return
        self.clf = KMeans(self.df, self.cs_k, 1)

    def init_centroids(self):
        x_min, x_max, y_min, y_max = self.get_data_limits()
        x, y = self.rng.uniform(size=(2, self.cs_k))
        x = (x_max - x_min) * x + x_min
        y = (y_max - y_min) * y + y_min
        self.clf.set_centroids(np.vstack((x, y)).T)

    def update_sr_points(self):
        x, y = self.df.f1.astype(float), self.df.f2.astype(float)
        self.sr_points.setData(x, y)
        color = self.clf.predict(self.df)
        self.sr_points.setPen(self.pens[color])
        self.sr_points.setBrush(self.brushes[color])

    def get_data_limits(self):
        x_min, x_max = self.df.f1.min(), self.df.f1.max()
        y_min, y_max = self.df.f2.min(), self.df.f2.max()
        return x_min, x_max, y_min, y_max

    def update_sr_centroids(self):
        x, y = self.clf.get_centroids()
        self.sr_centroids.setData(x, y)
        color = np.arange(self.cs_k)
        pens = [pg.mkPen(color='k')] * self.cs_k
        self.sr_centroids.setPen(pens)
        self.sr_centroids.setBrush(self.brushes[color])

    def cg_sl_k(self):
        self.init_centroids()
        self.update_sr_centroids()
        self.update_sr_points()

    def cg_ch_autorange(self):
        self.update_axes()

    def cg_ch_scale_axes(self):
        self.fig.setAspectLocked(self.cs_scale_axes)

    def ck_bt_run_at_once(self):
        if not self.valid:
            return
        self.do_step(300)
        self.update_sr_centroids()
        self.update_sr_points()

    def ck_bt_step(self):
        if not self.valid:
            return
        self.do_step(1)
        self.update_sr_centroids()
        self.update_sr_points()

    def do_step(self, n_steps):
        centroids = self.get_centroids()
        self.clf.set_params(centroids=centroids, max_iter=n_steps)
        self.clf.fit(self.df)

    def get_centroids(self):
        x, y = self.sr_centroids.getData()
        return np.vstack((x, y)).T

    def ck_sr_points(self, index):
        self.df = self.df.drop(self.df.index[index])
        self.update_after_df()
        self.update_sr_points()
        self.update_axes()

    def dr_sr_points(self, index, x, y):
        self.df.loc[index, ['f1', 'f2']] = x, y
        self.update_sr_points()

    def dre_sr_points(self, index, x, y):
        self.update_axes()

    def dr_sr_centroids(self, index, x, y):
        centroids = self.get_centroids()
        centroids[index] = x, y
        self.clf.set_centroids(centroids)
        self.update_sr_centroids()
        self.update_sr_points()

    def dre_sr_centroids(self, index, x, y):
        self.update_axes()

    def ck_sr_points_background(self, x, y):
        df_new_point = pd.DataFrame([(x, y)], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.update_sr_points()

    def update_axes(self):
        if self.cs_autorange:
            self.fig.autoRange()


if __name__ == "__main__":
    from Orange.widgets.utils.widgetpreview import WidgetPreview
    WidgetPreview(ValenciaKMeans).run()

