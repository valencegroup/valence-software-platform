import numpy as np
import pandas as pd
from scipy import special
from sklearn import linear_model

import pyqtgraph as pg
from AnyQt.QtCore import Qt, pyqtSignal as Signal, QPointF
from AnyQt.QtGui import QColor

from Orange.data import ContinuousVariable, DiscreteVariable, Domain, Table
from Orange.widgets import gui, settings
from Orange.widgets.settings import Setting
from Orange.widgets.utils.itemmodels import DomainModel
from Orange.widgets.widget import OWWidget, Input, Output, Msg
from Orange.widgets.utils.colorpalettes import DefaultRGBColors
from Orange.widgets.visualize.owscatterplotgraph import LegendItem


class LogisticRegression:
    def __init__(self, df):
        self.reg = linear_model.LogisticRegression()
        self.reg.fit(df[['x']], df['y'])

    def coeffs(self):
        return self.reg.intercept_[0], self.reg.coef_[0][0]


class LogisticRegressionPlotWidget(pg.PlotWidget):
    on_drag_points = Signal(int, float, float)
    on_hover_points = Signal(int, float, float)
    on_drag_end_points = Signal(int, float, float)
    on_click_points = Signal(int)
    on_background_click = Signal(float, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sr_points = None
        self.moved = False
        self.mouse_down = False
        self.index = None

    def set_sr_points(self, sr_points):
        self.sr_points = sr_points

    def mousePressEvent(self, ev):
        no_data = self.sr_points is None
        if ev.button() != Qt.LeftButton or no_data:
            ev.ignore()
            return
        ev.accept()
        self.mouse_down = True
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            self.index = self.sr_points.points().tolist().index(pts_points[0])

    def mouseReleaseEvent(self, ev):
        if ev.button() != Qt.LeftButton:
            ev.ignore()
            return
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        x, y = pos.x(), pos.y()
        if self.index is None:
            if not self.moved:
                self.on_background_click.emit(x, y)
        else:
            if self.moved:
                self.on_drag_end_points.emit(self.index, x, y)
            else:
                self.on_click_points.emit(self.index)
        self.index = None
        self.mouse_down = False
        self.moved = False

    def mouseMoveEvent(self, ev):
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            index = self.sr_points.points().tolist().index(pts_points[0])
            self.on_hover_points.emit(index, pos.x(), pos.y())
        if self.mouse_down and self.index is not None:
            self.moved = True
            self.on_drag_points.emit(self.index, pos.x(), pos.y())


class ValenciaLogisticRegression(OWWidget):
    name = 'Logistic Regression'
    description = 'Interactive application for logistic regression.'
    icon = 'icons/logreg.svg'
    priority = 100
    keywords = ['logistic', 'regression', 'interactive']
    want_main_area = True
    want_control_area = True
    resizing_enabled = True

    cs_x = settings.ContextSetting(None)
    cs_y = settings.ContextSetting(None)
    cs_autorange = settings.ContextSetting(True)
    cs_scale_axes = settings.ContextSetting(False)

    class Inputs:
        data = Input("Data", Table)

    class Outputs:
        data = Output("Data", Table, default=True)

    class Error(OWWidget.Error):
        n_features = Msg('Data must contain at least two numeric variables.')
        n_class = Msg('Data must contain at least one categorical variable.')
        n_class_var = Msg('Y variable must contain exactly two classes.')
        min_rows = Msg('Not enough data rows.')

    def __init__(self):
        super().__init__()
        self.rng = np.random.default_rng()
        self.colors = DefaultRGBColors.qcolors[np.arange(10)]
        self.max_n_classes = len(self.colors)
        self.pens = np.array([pg.mkPen(x.darker(120)) for x in self.colors])
        self.brushes = np.array([pg.mkBrush(x) for x in self.colors])
        self.create_app_controls()
        self.add_marks()
        self.bind_signals()
        self.set_data(None)

    def create_app_controls(self):
        self.controlArea.layout().setAlignment(Qt.AlignTop)
        self.dm_features = DomainModel(
            valid_types=(ContinuousVariable, ), order=DomainModel.MIXED)
        self.dm_class = DomainModel(
            valid_types=(DiscreteVariable, ), order=DomainModel.MIXED)
        gui.comboBox(
            self.controlArea, self, value='cs_x', model=self.dm_features,
            callback=self.ck_bt_reset, label='X:')
        gui.comboBox(
            self.controlArea, self, value='cs_y', model=self.dm_class,
            callback=self.ck_bt_reset, label='Y:')
        self.bt_reset = gui.button(
            self.controlArea, self, 'Reset', callback=self.ck_bt_reset)
        self.ch_autorange = gui.checkBox(
            self.controlArea, self, 'cs_autorange', label='Autorange:',
            callback=self.cg_ch_autorange)
        self.ch_scale_axes = gui.checkBox(
            self.controlArea, self, 'cs_scale_axes', label='Scale Axes:',
            callback=self.cg_ch_scale_axes)

    def add_marks(self):
        self.fig = LogisticRegressionPlotWidget()
        self.fig.disableAutoRange()
        self.mainArea.layout().addWidget(self.fig)
        self.lg_fig = LegendItem()
        self.lg_fig.setParentItem(self.fig.getViewBox())
        self.lg_fig.anchor((1, 0), (1, 0), offset=(-3, 1))
        self.ln_logistic = pg.PlotCurveItem()
        self.fig.addItem(self.ln_logistic)
        self.ln_logistic.setPen(self.pens[1])
        self.sr_points = pg.ScatterPlotItem(symbol='o', size=8)
        self.fig.addItem(self.sr_points)
        self.fig.set_sr_points(self.sr_points)

    def bind_signals(self):
        self.fig.on_drag_points.connect(self.dr_sr_points)
        self.fig.on_drag_end_points.connect(self.dre_sr_points)
        self.fig.on_click_points.connect(self.ck_sr_points)
        self.fig.on_background_click.connect(self.ck_sr_points_background)

    @Inputs.data
    def set_data(self, data):
        self.Error.clear()
        self.sr_points.setData([], [])
        self.ln_logistic.setData([], [])
        self.data_original = data
        if data is None:
            self.source_df = 'internal'
            self.dm_features.set_domain(None)
            self.dm_class.set_domain(None)
        else:
            self.source_df = 'external'
            self.dm_features.set_domain(data.domain)
            self.dm_class.set_domain(data.domain)
            if len(self.dm_features) < 1:
                self.Error.n_features()
                self.dm_features.set_domain(None)
                return
            if len(self.dm_class) < 1:
                self.Error.n_class()
                self.dm_class.set_domain(None)
                return
            self.cs_x = self.dm_features[0]
            self.cs_y = self.dm_class[0]
            self.reload_df_from_orange_table()
            self.check_arg_df_min_rows(self.df, 2)
        self.ck_bt_reset()

    def reload_df_from_orange_table(self):
        df_x, df_y = self.data_original.X_df, self.data_original.Y_df
        self.df = pd.concat([df_x, df_y], axis=1)
        self.df.reset_index(drop=True, inplace=True)

    def ck_bt_reset(self):
        column_names = ['x', 'y']
        if self.source_df == 'internal':
            self.x, self.y = column_names
            data = np.sort(self.rng.standard_normal(size=3))
            self.df = pd.DataFrame(data, columns=['x'])
            self.df['y'] = [0, 0, 1]
        elif self.source_df == 'external':
            self.reload_df_from_orange_table()
            self.x, self.y = self.cs_x.name, self.cs_y.name
            self.df = self.df[[self.x, self.y]]
            self.df.columns = column_names
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_logistic()

    def update_after_df(self):
        self.validate_df()

    def validate_df(self):
        self.Error.clear()
        self.valid = self.validate_df_runtime()

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            self.Error.min_rows()
            return False
        if self.df.y.unique().size != 2:
            self.Error.n_class_var()
            return False
        return True

    def check_arg_df_min_rows(self, df, n):
        if df.shape[0] < n:
            self.Error.min_rows()

    def update_legend(self, intercept, slope):
        self.lg_fig.clear()
        dot = pg.ScatterPlotItem(pen=self.pens[1], brush=self.brushes[1])
        sign = '+' if intercept >= 0 else '-'
        regression_line = f'{slope:.2f}x{sign}{abs(intercept):.2f}'
        text = f'y=1/(1+exp(-({slope:.2f}x{sign}{abs(intercept):.2f}))'
        self.lg_fig.addItem(dot, text)

    def reset_reg(self):
        if not self.valid:
            return
        self.reg = LogisticRegression(self.df)

    def update_sr_points(self):
        x, y = self.df.x.astype(float), self.df.y.astype(float)
        self.sr_points.setData(x, y)
        color = self.df.y.astype(int)
        self.sr_points.setPen(self.pens[color])
        self.sr_points.setBrush(self.brushes[color])

    def get_data_limits(self):
        intercept, slope = self.reg.coeffs()
        yl = np.log(99)  # zoom on 99% of the logistic curve
        xl = (np.array([yl, -yl]) - intercept) / slope
        x_min = min(self.df.x.min(), min(xl))
        x_max = max(self.df.x.max(), max(xl))
        y_min, y_max = -0.5, 1.5
        return x_min, x_max, y_min, y_max

    def project_ln_logistic(self):
        if not self.valid:
            return
        intercept, slope = self.reg.coeffs()
        (x_min, x_max), (y_min, y_max) = self.fig.viewRange()
        x = np.linspace(x_min, x_max, 120)
        y = special.expit(intercept + slope * x).ravel()
        self.ln_logistic.setData(x, y)
        self.update_legend(intercept, slope)

    def cg_ch_autorange(self):
        self.update_axes()

    def cg_ch_scale_axes(self):
        self.fig.setAspectLocked(self.cs_scale_axes)

    def dr_sr_points(self, index, x, y):
        self.df.loc[index, ['x', 'y']] = x, 1 if y > 0.5 else 0
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.project_ln_logistic()

    def dre_sr_points(self, index, x, y):
        self.update_axes()

    def ck_sr_points(self, index):
        self.df = self.df.drop(self.df.index[index])
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_logistic()

    def ck_sr_points_background(self, x, y):
        new_point = x, 1 if y > 0.5 else 0
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.project_ln_logistic()

    def update_axes(self):
        if self.cs_autorange:
            x_min, x_max, y_min, y_max = self.get_data_limits()
            self.fig.getViewBox().setRange(
                xRange=(x_min, x_max), yRange=(y_min, y_max))


if __name__ == "__main__":
    from Orange.widgets.utils.widgetpreview import WidgetPreview
    WidgetPreview(ValenciaLogisticRegression).run()

