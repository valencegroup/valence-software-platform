import numpy as np
import pandas as pd
from sklearn import neural_network
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning

import pyqtgraph as pg
from AnyQt.QtCore import Qt, pyqtSignal as Signal, QPointF
from AnyQt.QtGui import QColor

from Orange.data import ContinuousVariable, DiscreteVariable, Domain, Table
from Orange.widgets import gui, settings
from Orange.widgets.settings import Setting
from Orange.widgets.utils.itemmodels import DomainModel
from Orange.widgets.widget import OWWidget, Input, Output, Msg
from Orange.widgets.utils.colorpalettes import DefaultRGBColors
from Orange.widgets.visualize.owscatterplotgraph import LegendItem


class MLPClassifier:
    @ignore_warnings(category=ConvergenceWarning)
    def __init__(
            self,
            df,
            hiddens=(10,),
            alpha=0.01,
            learn=0.01,
            max_iter=50,
            ):
        self.clf = neural_network.MLPClassifier(
            hidden_layer_sizes=hiddens,
            alpha=alpha,
            learning_rate_init=learn,
            max_iter=max_iter,
            activation="relu",
            solver="sgd",
            tol=1e-9,
            random_state=42,
            )
        self.clf.fit(df[['f1', 'f2']], df.c)
        self.accuracy = self.clf.score(df[['f1', 'f2']], df.c)

    @ignore_warnings(category=ConvergenceWarning)
    def fit(self, df):
        self.clf.fit(df[['f1', 'f2']], df.c)
        self.accuracy = self.clf.score(df[['f1', 'f2']], df.c)

    def predict_proba(self, df):
        preds = self.clf.predict_proba(df[['f1', 'f2']])
        return preds

    def set_params(
            self,
            hiddens=None,
            alpha=None,
            learn=None,
            max_iter=None,
            ):
        if hiddens is not None:
            self.clf.set_params(hidden_layer_sizes=hiddens)
        if alpha is not None:
            self.clf.set_params(alpha=alpha)
        if learn is not None:
            self.clf.set_params(learning_rate_init=learn)
        if max_iter is not None:
            self.clf.set_params(max_iter=max_iter)


class NNClassificationPlotWidget(pg.PlotWidget):
    on_drag_points = Signal(int, float, float)
    on_hover_points = Signal(int, float, float)
    on_drag_end_points = Signal(int, float, float)
    on_click_points = Signal(int)
    on_background_click = Signal(float, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sr_points = None
        self.moved = False
        self.mouse_down = False
        self.index = None

    def set_sr_points(self, sr_points):
        self.sr_points = sr_points

    def mousePressEvent(self, ev):
        no_data = self.sr_points is None
        if ev.button() != Qt.LeftButton or no_data:
            ev.ignore()
            return
        ev.accept()
        self.mouse_down = True
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            self.index = self.sr_points.points().tolist().index(pts_points[0])

    def mouseReleaseEvent(self, ev):
        if ev.button() != Qt.LeftButton:
            ev.ignore()
            return
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        x, y = pos.x(), pos.y()
        if self.index is None:
            if not self.moved:
                self.on_background_click.emit(x, y)
        else:
            if self.moved:
                self.on_drag_end_points.emit(self.index, x, y)
            else:
                self.on_click_points.emit(self.index)
        self.index = None
        self.mouse_down = False
        self.moved = False

    def mouseMoveEvent(self, ev):
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            index = self.sr_points.points().tolist().index(pts_points[0])
            self.on_hover_points.emit(index, pos.x(), pos.y())
        if self.mouse_down and self.index is not None:
            self.moved = True
            self.on_drag_points.emit(self.index, pos.x(), pos.y())


class ValenciaNNClassification(OWWidget):
    name = 'NN Classification'
    description = 'Interactive application for neural network classification.'
    icon = 'icons/nnclass.svg'
    priority = 100
    keywords = ['neural', 'classification', 'interactive']
    want_main_area = True
    want_control_area = True
    resizing_enabled = True

    cs_f1 = settings.ContextSetting(None)
    cs_f2 = settings.ContextSetting(None)
    cs_c = settings.ContextSetting(None)
    cs_new_point_class = settings.ContextSetting(0)
    cs_h1 = settings.ContextSetting(5)
    cs_h2 = settings.ContextSetting(0)
    cs_h3 = settings.ContextSetting(0)
    cs_max_iter = settings.ContextSetting(100)
    cs_learn = settings.ContextSetting(0.1)
    cs_alpha = settings.ContextSetting(0.1)
    cs_autorange = settings.ContextSetting(True)
    cs_scale_axes = settings.ContextSetting(False)

    class Inputs:
        data = Input("Data", Table)

    class Outputs:
        data = Output("Data", Table, default=True)

    class Error(OWWidget.Error):
        n_features = Msg('Data must contain at least two numeric variables.')
        n_class = Msg('Data must contain at least one categorical variable.')
        n_class_var = Msg('Y variable must contain exactly two classes.')
        min_rows = Msg('Not enough data rows.')

    def __init__(self):
        super().__init__()
        self.rng = np.random.default_rng()
        self.colors = DefaultRGBColors.qcolors[np.arange(10)]
        self.max_n_classes = len(self.colors)
        self.pens = np.array([pg.mkPen(x.darker(120)) for x in self.colors])
        self.brushes = np.array([pg.mkBrush(x) for x in self.colors])
        self.create_app_controls()
        self.add_marks()
        self.bind_signals()
        self.set_data(None)

    def create_app_controls(self):
        self.controlArea.layout().setAlignment(Qt.AlignTop)
        self.dm_features = DomainModel(
            valid_types=(ContinuousVariable, ), order=DomainModel.MIXED)
        self.dm_class = DomainModel(
            valid_types=(DiscreteVariable, ), order=DomainModel.MIXED)
        gui.comboBox(
            self.controlArea, self, value='cs_f1', model=self.dm_features,
            callback=self.ck_bt_reset, label='Feature 1:')
        gui.comboBox(
            self.controlArea, self, value='cs_f2', model=self.dm_features,
            callback=self.ck_bt_reset, label='Feature 2:')
        gui.comboBox(
            self.controlArea, self, value='cs_c', model=self.dm_class,
            callback=self.ck_bt_reset, label='Class:')
        self.bt_reset = gui.button(
            self.controlArea, self, 'Reset', callback=self.ck_bt_reset)
        self.sl_h1 = gui.spin(
        self.controlArea, self, 'cs_h1', minv=1, maxv=10,
            label='HL 1:', callback=self.cg_sl_h)
        self.sl_h2 = gui.spin(
            self.controlArea, self, 'cs_h2', minv=0, maxv=10,
            label='HL 2:', callback=self.cg_sl_h)
        self.sl_h3 = gui.spin(
            self.controlArea, self, 'cs_h3', minv=0, maxv=10,
            label='HL 3:', callback=self.cg_sl_h)
        self.sl_max_iter = gui.spin(
            self.controlArea, self, 'cs_max_iter', minv=1, maxv=5000, step=100,
            label='Max iterations:', callback=self.cg_sl_max_iter)
        self.sl_learn = gui.doubleSpin(
            self.controlArea, self, 'cs_learn', minv=0.01, maxv=1, step=0.01,
            label='Learning rate:', callback=self.cg_sl_learn)
        self.sl_alpha = gui.doubleSpin(
            self.controlArea, self, 'cs_alpha', minv=0.001, maxv=1, step=0.001,
            label='Regularization:', callback=self.cg_sl_alpha)
        self.sl_new_point_class = gui.spin(
            self.controlArea, self, 'cs_new_point_class', minv=0, maxv=0,
            label='New Point Class:')
        self.ch_autorange = gui.checkBox(
            self.controlArea, self, 'cs_autorange', label='Autorange:',
            callback=self.cg_ch_autorange)
        self.ch_scale_axes = gui.checkBox(
            self.controlArea, self, 'cs_scale_axes', label='Scale Axes:',
            callback=self.cg_ch_scale_axes)

    def add_marks(self):
        self.fig = NNClassificationPlotWidget()
        self.fig.disableAutoRange()
        self.mainArea.layout().addWidget(self.fig)
        self.lg_fig = LegendItem()
        self.lg_fig.setParentItem(self.fig.getViewBox())
        self.lg_fig.anchor((1, 0), (1, 0))
        self.mlp_classifier = pg.ImageItem()
        self.fig.addItem(self.mlp_classifier)
        self.sr_points = pg.ScatterPlotItem(symbol='o', size=8)
        self.fig.addItem(self.sr_points)
        self.fig.set_sr_points(self.sr_points)

    def bind_signals(self):
        self.fig.on_drag_points.connect(self.dr_sr_points)
        self.fig.on_drag_end_points.connect(self.dre_sr_points)
        self.fig.on_click_points.connect(self.ck_sr_points)
        self.fig.on_background_click.connect(self.ck_sr_points_background)

    @Inputs.data
    def set_data(self, data):
        self.Error.clear()
        self.sr_points.setData([], [])
        self.mlp_classifier.setImage(None)
        self.data_original = data
        if data is None:
            self.source_df = 'internal'
            self.dm_features.set_domain(None)
            self.dm_class.set_domain(None)
        else:
            self.source_df = 'external'
            self.dm_features.set_domain(data.domain)
            self.dm_class.set_domain(data.domain)
            if len(self.dm_features) < 2:
                self.Error.n_features()
                self.dm_features.set_domain(None)
                self.dm_class.set_domain(None)
                return
            if len(self.dm_class) < 1:
                self.Error.n_class()
                self.dm_features.set_domain(None)
                self.dm_class.set_domain(None)
                return
            self.cs_f1, self.cs_f2 = self.dm_features[:2]
            self.cs_c = self.dm_class[0]
            self.reload_df_from_orange_table()
            self.check_arg_df_min_rows(self.df, 1)
        self.update_legend()
        self.ck_bt_reset()

    def reload_df_from_orange_table(self):
        df_x, df_y = self.data_original.X_df, self.data_original.Y_df
        self.df = pd.concat([df_x, df_y], axis=1)
        self.df.reset_index(drop=True, inplace=True)

    def ck_bt_reset(self):
        column_names = ['f1', 'f2', 'c']
        if self.source_df == 'internal':
            self.f1, self.f2, self.c = column_names
            data = self.rng.standard_normal(size=(5, 2))
            self.df = pd.DataFrame(data, columns=column_names[:2])
            self.df[self.c] = [0, 0, 0, 1, 1]
        elif self.source_df == 'external':
            self.reload_df_from_orange_table()
            self.f1, self.f2 = self.cs_f1.name, self.cs_f2.name
            self.c = self.cs_c.name
            self.df = self.df[[self.f1, self.f2, self.c]]
            self.df.columns = column_names
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_mlp_classifier()

    def update_after_df(self):
        self.validate_df()
        n_classes_max = max(self.df.c.astype(int), default=-1)
        self.sl_new_point_class.setMaximum(n_classes_max)

    def validate_df(self):
        self.Error.clear()
        self.valid = self.validate_df_runtime()

    def validate_df_runtime(self):
        if self.df.shape[0] < 1:
            self.Error.min_rows()
            return False
        if self.df.c.unique().size != 2:
            self.Error.n_class_var()
            return False
        return True

    def check_arg_df_min_rows(self, df, n):
        if df.shape[0] < n:
            self.Error.min_rows()

    def update_legend(self):
        self.lg_fig.clear()
        if self.source_df == 'internal':
            return
        for i, class_name in enumerate(self.cs_c.values):
            color = self.colors[i]
            dot = pg.ScatterPlotItem(pen=self.pens[i], brush=self.brushes[i])
            self.lg_fig.addItem(dot, class_name)

    def reset_clf(self):
        hiddens = [self.cs_h1, self.cs_h2, self.cs_h3]
        hiddens = [int(h) for h in hiddens if h > 0]
        self.clf = MLPClassifier(
            self.df,
            hiddens=hiddens,
            alpha=self.cs_alpha,
            learn=self.cs_learn,
            max_iter=self.cs_max_iter,
            )

    def update_sr_points(self):
        x, y = self.df.f1.astype(float), self.df.f2.astype(float)
        self.sr_points.setData(x, y)
        color = self.df.c.astype(int)
        self.sr_points.setPen(self.pens[color])
        self.sr_points.setBrush(self.brushes[color])

    def get_data_limits(self):
        x_min, x_max = self.df.f1.min(), self.df.f1.max()
        y_min, y_max = self.df.f2.min(), self.df.f2.max()
        return x_min, x_max, y_min, y_max

    def project_mlp_classifier(self):
        if not self.valid:
            return
        (x_min, x_max), (y_min, y_max) = self.fig.viewRange()
        h = 100
        x, y = np.linspace(x_min, x_max, h), np.linspace(y_min, y_max, h)
        xx0, xx1 = np.meshgrid(x, y)
        feats = np.column_stack([xx0.ravel(), xx1.ravel()])
        df = pd.DataFrame({"f1": feats[:, 0], "f2": feats[:, 1]})
        Z = self.clf.predict_proba(df)[:, 1]
        Z = Z.reshape(xx0.shape)
        self.mlp_classifier.setImage(Z)
        self.mlp_classifier.setRect(x_min, y_min, x_max-x_min, y_max-y_min)
        self.mlp_classifier.setLevels([0, 1])
        colors = [
            self.colors[0],
            (255, 255, 255),
            self.colors[1]]
        cmap = pg.ColorMap(pos=np.linspace(0, 1, 3), color=colors)
        self.mlp_classifier.setLookupTable(cmap.getLookupTable())
        self.mlp_classifier.setOpts(opacity=0.5)

    def cg_sl_h(self):
        hiddens = [self.cs_h1, self.cs_h2, self.cs_h3]
        hiddens = [h for h in hiddens if h > 0]
        self.clf.set_params(hiddens=hiddens)
        self.clf.fit(self.df)
        self.project_mlp_classifier()

    def cg_sl_max_iter(self):
        self.clf.set_params(max_iter=self.cs_max_iter)
        self.clf.fit(self.df)
        self.project_mlp_classifier()

    def cg_sl_learn(self):
        self.clf.set_params(learn=self.cs_max_iter)
        self.clf.fit(self.df)
        self.project_mlp_classifier()

    def cg_sl_alpha(self):
        self.clf.set_params(alpha=self.cs_max_iter)
        self.clf.fit(self.df)
        self.project_mlp_classifier()

    def cg_ch_autorange(self):
        self.update_axes()

    def cg_ch_scale_axes(self):
        self.fig.setAspectLocked(self.cs_scale_axes)

    def ck_sr_points(self, index):
        self.df = self.df.drop(self.df.index[index])
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_mlp_classifier()

    def dr_sr_points(self, index, x, y):
        self.df.loc[index, ['f1', 'f2']] = x, y
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.project_mlp_classifier()

    def dre_sr_points(self, index, x, y):
        self.update_axes()

    def ck_sr_points_background(self, x, y):
        new_point = x, y, self.cs_new_point_class
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_mlp_classifier()

    def update_axes(self):
        if self.cs_autorange:
            self.fig.autoRange(items=[self.sr_points])


if __name__ == "__main__":
    from Orange.widgets.utils.widgetpreview import WidgetPreview
    WidgetPreview(ValenciaNNClassification.run())

