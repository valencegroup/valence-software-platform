import numpy as np
import pandas as pd
from sklearn import linear_model, preprocessing

import pyqtgraph as pg
from AnyQt.QtCore import Qt, pyqtSignal as Signal, QPointF
from AnyQt.QtGui import QColor

from Orange.data import ContinuousVariable, DiscreteVariable, Domain, Table
from Orange.widgets import gui, settings
from Orange.widgets.settings import Setting
from Orange.widgets.utils.itemmodels import DomainModel
from Orange.widgets.widget import OWWidget, Input, Output, Msg
from Orange.widgets.utils.colorpalettes import DefaultRGBColors
from Orange.widgets.visualize.owscatterplotgraph import LegendItem


class Perceptron:
    def __init__(self, df, eta):
        self.clf = linear_model.Perceptron(eta0=eta)
        self.clf.partial_fit(df[['f1', 'f2']], df.c, classes=df.c.unique())

    def fit(self, df):
        self.clf.fit(df[['f1', 'f2']], df.c)

    def partial_fit(self, df):
        self.clf.partial_fit(df[['f1', 'f2']], df.c)

    def set_params(self, eta):
        self.clf.set_params(eta0=eta)

    def coeffs(self, n_class=0):
        n_class = 0 if self.clf.classes_.size == 2 else n_class
        return self.clf.intercept_[n_class], self.clf.coef_[n_class]


class PerceptronPlotWidget(pg.PlotWidget):
    on_drag_points = Signal(int, float, float)
    on_hover_points = Signal(int, float, float)
    on_drag_end_points = Signal(int, float, float)
    on_click_points = Signal(int)
    on_background_click = Signal(float, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sr_points = None
        self.moved = False
        self.mouse_down = False
        self.index = None

    def set_sr_points(self, sr_points):
        self.sr_points = sr_points

    def mousePressEvent(self, ev):
        no_data = self.sr_points is None
        if ev.button() != Qt.LeftButton or no_data:
            ev.ignore()
            return
        ev.accept()
        self.mouse_down = True
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            self.index = self.sr_points.points().tolist().index(pts_points[0])

    def mouseReleaseEvent(self, ev):
        if ev.button() != Qt.LeftButton:
            ev.ignore()
            return
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        x, y = pos.x(), pos.y()
        if self.index is None:
            if not self.moved:
                self.on_background_click.emit(x, y)
        else:
            if self.moved:
                self.on_drag_end_points.emit(self.index, x, y)
            else:
                self.on_click_points.emit(self.index)
        self.index = None
        self.mouse_down = False
        self.moved = False

    def mouseMoveEvent(self, ev):
        ev.accept()
        pos = self.plotItem.mapToView(QPointF(ev.pos()))
        pts_points = self.sr_points.pointsAt(pos)
        if len(pts_points) > 0:
            index = self.sr_points.points().tolist().index(pts_points[0])
            self.on_hover_points.emit(index, pos.x(), pos.y())
        if self.mouse_down and self.index is not None:
            self.moved = True
            self.on_drag_points.emit(self.index, pos.x(), pos.y())


class ValenciaPerceptron(OWWidget):
    name = 'Perceptron'
    description = 'Interactive application for the perceptron.'
    icon = 'icons/perceptron.svg'
    priority = 100
    keywords = ['perceptron', 'interactive']
    want_main_area = True
    want_control_area = True
    resizing_enabled = True

    cs_f1 = settings.ContextSetting(None)
    cs_f2 = settings.ContextSetting(None)
    cs_c = settings.ContextSetting(None)
    cs_predict_for = settings.ContextSetting(0)
    cs_eta = settings.ContextSetting(0.1)
    cs_new_point_class = settings.ContextSetting(0)
    cs_autorange = settings.ContextSetting(True)
    cs_scale_axes = settings.ContextSetting(False)

    class Inputs:
        data = Input("Data", Table)

    class Outputs:
        data = Output("Data", Table, default=True)

    class Error(OWWidget.Error):
        n_features = Msg('Data must contain at least two numeric variables.')
        n_class = Msg('Data must contain at least one categorical variable.')
        min_rows = Msg('Not enough data rows.')

    def __init__(self):
        super().__init__()
        self.rng = np.random.default_rng()
        self.colors = DefaultRGBColors.qcolors[np.arange(10)]
        self.max_n_classes = len(self.colors)
        self.pens = np.array([pg.mkPen(x.darker(120)) for x in self.colors])
        self.brushes = np.array([pg.mkBrush(x) for x in self.colors])
        self.create_app_controls()
        self.add_marks()
        self.bind_signals()
        self.set_data(None)

    def create_app_controls(self):
        self.controlArea.layout().setAlignment(Qt.AlignTop)
        self.dm_features = DomainModel(
            valid_types=(ContinuousVariable, ), order=DomainModel.MIXED)
        self.dm_class = DomainModel(
            valid_types=(DiscreteVariable, ), order=DomainModel.MIXED)
        gui.comboBox(
            self.controlArea, self, value='cs_f1', model=self.dm_features,
            callback=self.ck_bt_reset, label='Feature 1:')
        gui.comboBox(
            self.controlArea, self, value='cs_f2', model=self.dm_features,
            callback=self.ck_bt_reset, label='Feature 2:')
        gui.comboBox(
            self.controlArea, self, value='cs_c', model=self.dm_class,
            callback=self.ck_bt_reset, label='Class:')
        self.bt_reset = gui.button(
            self.controlArea, self, 'Reset', callback=self.ck_bt_reset)
        self.sl_new_point_class = gui.spin(
            self.controlArea, self, 'cs_new_point_class', minv=0, maxv=0,
            label='New Point Class:')
        self.sl_predict_for = gui.spin(
            self.controlArea, self, 'cs_predict_for', minv=0, maxv=0,
            label='Predict for:', callback=self.cg_sl_predict_for)
        self.sl_eta = gui.doubleSpin(
            self.controlArea, self, 'cs_eta', minv=0.1, maxv=1, step=0.01,
            label='η:', callback=self.cg_sl_eta)
        self.bt_run_at_once = gui.button(
            self.controlArea, self, 'Run At Once',
            callback=self.ck_bt_run_at_once)
        self.bt_step = gui.button(
            self.controlArea, self, 'Step', callback=self.ck_bt_step)
        self.ch_autorange = gui.checkBox(
            self.controlArea, self, 'cs_autorange', label='Autorange:',
            callback=self.cg_ch_autorange)
        self.ch_scale_axes = gui.checkBox(
            self.controlArea, self, 'cs_scale_axes', label='Scale Axes:',
            callback=self.cg_ch_scale_axes)

    def add_marks(self):
        self.fig = PerceptronPlotWidget()
        self.fig.disableAutoRange()
        self.mainArea.layout().addWidget(self.fig)
        self.lg_fig = LegendItem()
        self.lg_fig.setParentItem(self.fig.getViewBox())
        self.lg_fig.anchor((1, 0), (1, 0))
        self.sr_points = pg.ScatterPlotItem(symbol='o', size=8)
        self.fig.addItem(self.sr_points)
        self.fig.set_sr_points(self.sr_points)
        self.ln_separation = pg.PlotCurveItem()
        self.fig.addItem(self.ln_separation)

    def bind_signals(self):
        self.fig.on_drag_points.connect(self.dr_sr_points)
        self.fig.on_drag_end_points.connect(self.dre_sr_points)
        self.fig.on_click_points.connect(self.ck_sr_points)
        self.fig.on_background_click.connect(self.ck_sr_points_background)

    @Inputs.data
    def set_data(self, data):
        self.Error.clear()
        self.sr_points.setData([], [])
        self.ln_separation.setData([], [])
        self.data_original = data
        if data is None:
            self.source_df = 'internal'
            self.dm_features.set_domain(None)
            self.dm_class.set_domain(None)
        else:
            self.source_df = 'external'
            self.dm_features.set_domain(data.domain)
            self.dm_class.set_domain(data.domain)
            if len(self.dm_features) < 2:
                self.Error.n_features()
                self.dm_features.set_domain(None)
                self.dm_class.set_domain(None)
                return
            if len(self.dm_class) < 1:
                self.Error.n_class()
                self.dm_features.set_domain(None)
                self.dm_class.set_domain(None)
                return
            self.cs_f1, self.cs_f2 = self.dm_features[:2]
            self.cs_c = self.dm_class[0]
            self.reload_df_from_orange_table()
            self.check_arg_df_min_rows(self.df, 2)
        self.update_legend()
        self.ck_bt_reset()

    def reload_df_from_orange_table(self):
        df_x, df_y = self.data_original.X_df, self.data_original.Y_df
        self.df = pd.concat([df_x, df_y], axis=1)
        self.df.reset_index(drop=True, inplace=True)

    def ck_bt_reset(self):
        column_names = ['f1', 'f2', 'c']
        if self.source_df == 'internal':
            self.f1, self.f2, self.c = column_names
            data = self.rng.standard_normal(size=(3, 2))
            self.df = pd.DataFrame(data, columns=column_names[:2])
            self.df[self.c] = [0, 0, 1]
        elif self.source_df == 'external':
            self.reload_df_from_orange_table()
            self.f1, self.f2 = self.cs_f1.name, self.cs_f2.name
            self.c = self.cs_c.name
            self.df = self.df[[self.f1, self.f2, self.c]]
            self.df.columns = column_names
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_separation()

    def update_after_df(self):
        self.validate_df()
        n_classes_max = max(self.df.c.astype(int), default=-1)
        self.sl_new_point_class.setMaximum(n_classes_max + 1)
        self.sl_predict_for.setMaximum(max(0, n_classes_max))

    def validate_df(self):
        self.Error.clear()
        self.valid = self.validate_df_runtime()

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            self.Error.min_rows()
            return False
        return True

    def check_arg_df_min_rows(self, df, n):
        if df.shape[0] < n:
            self.Error.min_rows()

    def update_legend(self):
        self.lg_fig.clear()
        if self.source_df == 'internal':
            return
        for i, class_name in enumerate(self.cs_c.values):
            color = self.colors[i]
            dot = pg.ScatterPlotItem(pen=self.pens[i], brush=self.brushes[i])
            self.lg_fig.addItem(dot, class_name)

    def reset_clf(self):
        if not self.valid:
            return
        self.clf = Perceptron(self.df, self.cs_eta)

    def update_sr_points(self):
        x, y = self.df.f1.astype(float), self.df.f2.astype(float)
        self.sr_points.setData(x, y)
        color = self.df.c.astype(int)
        self.sr_points.setPen(self.pens[color])
        self.sr_points.setBrush(self.brushes[color])

    def get_data_limits(self):
        x_min, x_max = self.df.f1.min(), self.df.f1.max()
        y_min, y_max = self.df.f2.min(), self.df.f2.max()
        return x_min, x_max, y_min, y_max

    def project_ln_separation(self):
        if not self.valid:
            return
        (x_min, x_max), (y_min, y_max) = self.fig.viewRange()
        bias, weights = self.clf.coeffs(self.cs_predict_for)
        intercept, slope = -bias / weights[1], -weights[0] / weights[1]
        if abs(np.tan(slope)) <= 1:
            x = np.linspace(x_min, x_max, 2)
            y = intercept + slope * x
        else:
            intercept, slope = -bias / weights[0], -weights[1] / weights[0]
            y = np.linspace(y_min, y_max, 2)
            x = intercept + slope * y
        self.ln_separation.setData(x, y)
        color = self.cs_predict_for
        self.ln_separation.setPen(self.pens[color])

    def cg_sl_predict_for(self):
        color = self.cs_predict_for
        self.ln_separation.setPen(self.pens[color])

    def cg_sl_eta(self):
        self.clf.set_params(self.cs_eta)

    def ck_bt_run_at_once(self):
        if not self.valid:
            return
        self.clf.fit(self.df)
        self.project_ln_separation()

    def ck_bt_step(self):
        if not self.valid:
            return
        df = self.df
        self.clf.partial_fit(df)
        self.project_ln_separation()

    def cg_ch_autorange(self):
        self.update_axes()

    def cg_ch_scale_axes(self):
        self.fig.setAspectLocked(self.cs_scale_axes)

    def ck_sr_points(self, index):
        self.df = self.df.drop(self.df.index[index])
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_separation()

    def dr_sr_points(self, index, x, y):
        self.df.loc[index, ['f1', 'f2']] = x, y
        self.update_after_df()
        self.update_sr_points()
        self.project_ln_separation()

    def dre_sr_points(self, index, x, y):
        self.reset_clf()
        self.update_axes()

    def ck_sr_points_background(self, x, y):
        new_point = x, y, self.cs_new_point_class
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_separation()

    def update_axes(self):
        if self.cs_autorange:
            self.fig.autoRange(items=[self.sr_points])


if __name__ == "__main__":
    from Orange.widgets.utils.widgetpreview import WidgetPreview
    WidgetPreview(ValenciaPerceptron).run()

