import os
import time
import json
import numpy as np
import pandas as pd
from scipy import special
from sklearn import preprocessing
from sklearn import neighbors
from sklearn import linear_model
from sklearn import tree
from sklearn import cluster
from sklearn import neural_network
# imports to silence ConvergenceWarning
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
from sklearn.datasets import make_moons
import ipywidgets as ipw
import bqplot as bq
import graphviz as gv
from IPython import display


MSG_ERROR = """
    <style>
    .alert {
        font-size: 18px;
        padding: 18px;
        background-color: #f44336;
        color: white;
        max-width: 300px;
    }
    </style>
    <div class="alert">
        <strong>Error!</strong> MESSAGE
    </div>
"""


class ValetudoBase:
    def __init__(self):
        self.rng = np.random.default_rng()
        self.create_ui()

    def create_ui(self):
        self.create_controls()
        self.create_fig()
        self.bind_signals()

    def create_controls(self):
        ly_bt = ipw.Layout(width='40px')
        self.tb_pan_zoom = ipw.ToggleButton(
            icon='arrows', tooltip='Pan & Zoom', layout=ly_bt)
        self.tb_scale_axes = ipw.ToggleButton(
            icon='balance-scale', tooltip='Scale Axes', layout=ly_bt)
        self.tb_autorange = ipw.ToggleButton(
            icon='expand', tooltip='Autorange', value=True, layout=ly_bt)
        self.bt_refresh = ipw.Button(
            icon='refresh', tooltip='Refresh', layout=ly_bt)
        self.bt_fig_minus = ipw.Button(
            icon='minus', tooltip='Shrink Figure', layout=ly_bt)
        self.bt_fig_plus = ipw.Button(
            icon='plus', tooltip='Expand Figure', layout=ly_bt)
        self.tb_animate = ipw.ToggleButton(
            icon='gears', tooltip='Animate', value=True, layout=ly_bt)
        self.bt_save_png = ipw.Button(
            icon='file-image', tooltip='Save as .png', layout=ly_bt)
        self.tb_legend = ipw.ToggleButton(
            icon='info', tooltip='Show Legend', value=True, layout=ly_bt)
        self.ht_classes = ipw.HTML(layout=ipw.Layout(margin='30px 0 0 0'))
        self.fig_controls = [
            self.tb_pan_zoom, self.tb_scale_axes, self.tb_autorange,
            self.bt_refresh, self.bt_fig_minus, self.bt_fig_plus,
            self.tb_animate, self.bt_save_png, self.tb_legend]

    def create_fig(self):
        self.colors = bq.CATEGORY10
        self.sc_x = bq.LinearScale(allow_padding=False)
        self.sc_y = bq.LinearScale(allow_padding=False)
        self.sc_c = bq.OrdinalColorScale(colors=self.colors)
        self.max_n_classes = len(self.colors)
        self.scs_x_y = {'x': self.sc_x, 'y': self.sc_y}
        self.scs_x_y_c = {'x': self.sc_x, 'y': self.sc_y, 'color': self.sc_c}
        self.ax_x = bq.Axis(scale=self.sc_x)
        self.ax_y = bq.Axis(scale=self.sc_y, orientation='vertical')
        self.ax_c = bq.ColorAxis(
            scale=self.sc_c, orientation='vertical', side='right')
        self.axs_x_y = [self.ax_x, self.ax_y]
        self.axs_x_y_c = [self.ax_x, self.ax_y, self.ax_c]
        self.tt_point_loc = bq.Tooltip(
            fields=['x', 'y'], formats=['.2f', '.2f'])
        self.pan_zoom = bq.PanZoom(
            scales={'x': [self.sc_x], 'y': [self.sc_y]})
        self.fig_size = 750
        fig_size_str = f'{self.fig_size}px'
        self.fig = bq.Figure(
            axes=self.axs_x_y,
            min_aspect_ratio=1, max_aspect_ratio=1,
            layout={'width': fig_size_str, 'height': fig_size_str})
        self.set_animations(True)

    def bind_signals(self):
        self.tb_pan_zoom.observe(self.cg_tb_pan_zoom, 'value')
        self.tb_scale_axes.observe(self.cg_tb_scale_axes, 'value')
        self.tb_autorange.observe(self.cg_tb_autorange, 'value')
        self.bt_refresh.on_click(self.ck_bt_refresh)
        self.bt_fig_minus.on_click(self.ck_bt_fig_minus)
        self.bt_fig_plus.on_click(self.ck_bt_fig_plus)
        self.tb_animate.observe(self.cg_tb_animate, 'value')
        self.bt_save_png.on_click(self.ck_bt_save_png)
        self.tb_legend.observe(self.cg_tb_legend, 'value')

    def cg_tb_pan_zoom(self, change):
        self.fig.interaction = self.pan_zoom if change['new'] else None

    def cg_tb_scale_axes(self, change):
        self.update_axes(force=True)

    def cg_tb_autorange(self, change):
        if change['new']:
            self.update_axes()

    def ck_bt_refresh(self, bt_refresh):
        self.update_axes(force=True)

    def ck_bt_fig_minus(self, bt_fig_minus):
        self.update_fig_size(-50)

    def ck_bt_fig_plus(self, bt_fig_plus):
        self.update_fig_size(50)

    def update_fig_size(self, delta):
        self.fig_size += delta
        self.fig_size = max(500, min(1000, self.fig_size))
        self.fig.layout.width = self.fig.layout.height = f'{self.fig_size}px'

    def cg_tb_animate(self, change):
        self.set_animations(change['new'])

    def ck_bt_save_png(self, bt_save_png):
        self.fig.save_png()

    def cg_tb_legend(self, change):
        self.set_info(change['new'])

    def set_animations(self, switch, duration=500, action=None):
        # fix for temporary animation disabling
        # while dragging and deleting points
        if action == 'store':
            self.stored_animation_duration = self.fig.animation_duration
        self.fig.animation_duration = duration if switch else 0
        if action == 'restore':
            self.fig.animation_duration = self.stored_animation_duration

    def set_limits(self, x_min, x_max, y_min, y_max):
        dx, dy = x_max - x_min, y_max - y_min
        if dx == 0:
            x_min, x_max = x_min - 0.5, x_max + 0.5
        if dy == 0:
            y_min, y_max = y_min - 0.5, y_max + 0.5
        with self.sc_x.hold_sync():
            self.sc_x.min, self.sc_x.max = x_min, x_max
        with self.sc_y.hold_sync():
            self.sc_y.min, self.sc_y.max = y_min, y_max

    def expand_limits(self, x_min, x_max, y_min, y_max, factor=0.2):
        dx, dy = x_max - x_min, y_max - y_min
        x_min, x_max = x_min - dx*factor, x_max + dx*factor
        y_min, y_max = y_min - dy*factor, y_max + dy*factor
        if self.tb_scale_axes.value:
            dx, dy = x_max - x_min, y_max - y_min
            delta = abs(dx - dy) / 2
            if dx > dy:
                y_min, y_max = y_min - delta, y_max + delta
            elif dx < dy:
                x_min, x_max = x_min - delta, x_max + delta
        return x_min, x_max, y_min, y_max

    def get_color_for_n(self, n):
        return self.colors[n % self.max_n_classes]

    def wait_for_animation(self):
        time.sleep(self.fig.animation_duration/1000)

    def update_ht_classes(self):
        one_class = '<span style="color:{}">⬤</span> {}<br>'
        classes_text = '<h2>Classes</h2>'
        for index, class_name in enumerate(self.le.classes_):
            color = self.get_color_for_n(index)
            classes_text += one_class.format(color, class_name)
        self.ht_classes.value = classes_text

    def check_arg_df(self, df):
        if type(df) is not pd.DataFrame:
            raise ValueError(f'{type(df)} passed instead of pandas.DataFrame.')

    def check_arg_df_min_rows(self, df, n):
        self.check_arg_df(df)
        if df.shape[0] < n:
            raise ValueError(
                f'Insufficient data. At least {n} rows are required.')

    def check_arg_col(self, df, col):
        if type(col) is not str:
            raise TypeError(f'Invalid column argument: {col}.')
        if col not in df.columns:
            raise ValueError(f'Invalid column name: {col}.')

    def check_arg_col_numeric(self, df, col):
        self.check_arg_col(df, col)
        if not pd.api.types.is_numeric_dtype(df[col]):
            raise ValueError(f'Column {col} has non-numeric values.')

    def _ipython_display_(self):
        bx_control_fig = ipw.HBox(
            self.fig_controls, layout={'justify_content': 'center'})
        ly_bx_control_app = {
            'width': '350px', 'align_items': 'center',
            'justify_content': 'center'}
        self.bx_control_app = ipw.VBox(
            self.app_controls + [self.ht_classes], layout=ly_bx_control_app)
        bx_fig = ipw.VBox([bx_control_fig, self.fig])
        bx_body = ipw.HBox([bx_fig, self.bx_control_app])
        bx_main = ipw.VBox([self.ht_title, bx_body])
        display.display(bx_main)


class KNearestNeighbors:
    def __init__(self, df, k=3):
        self.clf = neighbors.KNeighborsClassifier(k)
        self.clf.fit(df[['f1', 'f2']], df.c)

    def get_data(self, x, y):
        data = pd.DataFrame([[x, y]], columns=['f1', 'f2'])
        prediction = self.clf.predict(data).astype(int)
        radii, indexes = self.clf.kneighbors(data)
        radius, indexes = radii[0][-1], indexes[0]
        return prediction, radius, indexes


class ValetudoKNearestNeighbors(ValetudoBase):
    """Interactive application for k-nearest neighbors.

    Study the basics of the k-nearest neighbors classifier.
    Drag the cross point to demonstrate how the nearest neighbors are
    highlighted. You can add, move, or delete points interactively.

    Parameters
    ----------
    df: pandas.DataFrame, optional
        A pandas dataframe containing the data for the interactive
        application. If this argument is omitted, a random set of points
        is generated instead. The columns to be utilized should be provided
        in the appropriate arguments. Rows with not-a-numbers will be removed.
    f1, f2: str, optional
        If a dataframe is provided via `df`, `f1` and `f2` are the column
        names of the features that would be used in the interactive
        application. The data in these columns has to be numeric.
    c: str, optional
        If a dataframe is provided via `df`, `c` is the column name of the
        classes that would be used in the interactive application. The values
        will be label encoded.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, or the dataframe is of
        insufficient length.
    """

    def __init__(
        self, df: pd.DataFrame = None,
        f1: str = '', f2: str = '', c: str = ''
    ):
        super().__init__()
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, f1, f2, c)
        self.reset_clf()
        self.update_sr_points()
        self.update_sr_center()
        self.update_axes(force=True)
        self.tb_scale_axes.value = True

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>k-Nearest Neighbors</h1>')
        self.bt_reset = ipw.Button(description='Reset', button_style='danger')
        self.sl_k = ipw.IntSlider(value=1, min=1, max=1, description='K:')
        self.sl_new_point_class = ipw.IntSlider(
            value=0, min=0, max=0, description='New Point Class:',
            style={'description_width': 'initial'})
        self.ht_error = ipw.HTML()
        self.ou_classes = ipw.Output()
        self.app_controls = [
            self.bt_reset, self.sl_k, self.sl_new_point_class, self.ht_error,
            self.ou_classes]

    def add_marks(self):
        self.ln_circle = bq.Lines(
            x=[], y=[], scales=self.scs_x_y_c, labels='k-Nearest Neighbors')
        self.sr_points = bq.Scatter(
            x=[], y=[], enable_move=True, scales=self.scs_x_y_c,
            interactions={'click': 'add', 'hover': 'tooltip'},
            selected_style={'stroke': 'black'}, tooltip=self.tt_point_loc,
            unselected_style={'opacity': 0.5}, labels='Points')
        self.sr_center = bq.Scatter(
            x=[], y=[], colors='black', scales=self.scs_x_y, enable_move=True,
            marker='cross', labels='A New Point')
        self.fig.marks = [self.ln_circle, self.sr_points, self.sr_center]
        self.fig.axes = self.axs_x_y_c
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_reset.on_click(self.ck_bt_reset)
        self.sl_k.observe(self.cg_sl_k, 'value')
        self.sl_new_point_class.observe(self.cg_sl_new_point_class, 'value')
        self.sr_center.on_drag_start(self.dr_sr_center)
        self.sr_center.on_drag(self.dr_sr_center)
        self.sr_center.on_drag_end(self.dr_sr_center)
        self.sr_points.on_drag_start(self.dr_sr_points)
        self.sr_points.on_drag(self.dr_sr_points)
        self.sr_points.on_drag_end(self.dr_sr_points)
        self.sr_points.on_background_click(self.ck_sr_points_background)
        ipw.dlink(
            (self.sl_new_point_class, 'value'),
            (self.sl_new_point_class.style, 'handle_color'),
            transform=self.get_color_for_n)

    def init_df(self, df, f1, f2, c):
        column_names = ['f1', 'f2', 'c']
        if df is None:
            self.f1, self.f2, self.c = column_names
            data = self.rng.standard_normal(size=(5, 2))
            self.df = pd.DataFrame(data, columns=column_names[:2])
            self.df[self.c] = [0, 0, 0, 1, 1]
            self.source_df = 'internal'
            self.df_original = self.df.copy()
            self.update_after_df()
            return
        self.check_arg_df(df)
        [self.check_arg_col_numeric(df, col) for col in [f1, f2]]
        self.check_arg_col(df, c)
        self.df = df[[f1, f2, c]].copy().dropna()
        self.check_arg_df_min_rows(self.df, 1)
        self.df.columns = column_names
        self.f1, self.f2, self.c = f1, f2, c
        self.le = preprocessing.LabelEncoder()
        self.df.c = self.le.fit_transform(self.df.c)
        self.update_ht_classes()
        self.source_df = 'external'
        self.df_original = self.df.copy()
        self.update_after_df()

    def update_after_df(self):
        self.validate_df()
        self.ax_x.label, self.ax_y.label = self.f1, self.f2
        self.tt_point_loc.labels = [self.f1, self.f2]
        n_classes_max = max(self.df.c, default=-1)
        self.sl_new_point_class.max = n_classes_max + 1
        self.sl_k.max = min(30, max(1, self.df.shape[0]))
        self.sc_c.domain = list(range(n_classes_max + 1))

    def validate_df(self):
        self.valid, msg = self.validate_df_runtime()
        error = MSG_ERROR.replace('MESSAGE', msg)
        self.ht_error.value = error if not self.valid else ''

    def validate_df_runtime(self):
        if self.df.shape[0] < 1:
            msg = 'Not enough data points.'
            msg += ' At least 1 is required. Place some points.'
            return False, msg
        return True, ''

    def reset_clf(self):
        if not self.valid:
            return
        self.clf = KNearestNeighbors(self.df, k=self.sl_k.value)

    def update_sr_points(self):
        with self.sr_points.hold_sync():
            self.sr_points.x = self.df.f1.astype(float)
            self.sr_points.y = self.df.f2.astype(float)
            self.sr_points.color = self.df.c
        self.preplace_color_for_new_point()

    def update_axes(self, force=False):
        if not self.valid:
            return
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits(*limits)
        self.set_limits(*limits)

    def get_data_limits(self):
        x_min, x_max = self.df.f1.min(), self.df.f1.max()
        y_min, y_max = self.df.f2.min(), self.df.f2.max()
        xc, yc = self.sr_center.x[0], self.sr_center.y[0]
        x_min, x_max = min(x_min, xc), max(x_max, xc)
        y_min, y_max = min(y_min, yc), max(y_max, yc)
        return x_min, x_max, y_min, y_max

    def update_sr_center(self, xc=None, yc=None):
        if not self.valid:
            return
        if xc is None:
            dx = self.df.f1.max() - self.df.f1.min()
            dx = dx if dx != 0 else 1
            xc = self.df.f1.min() - 0.2*dx
        if yc is None:
            dy = self.df.f2.max() - self.df.f2.min()
            dy = dy if dy != 0 else 1
            yc = self.df.f2.min() - 0.2*dy
        with self.sr_center.hold_sync():
            self.sr_center.x, self.sr_center.y = [xc], [yc]
        self.update_ln_circle_same_center()

    def update_ln_circle_same_center(self):
        xc, yc = self.sr_center.x[0], self.sr_center.y[0]
        self.update_ln_circle(xc, yc)

    def update_ln_circle(self, xc, yc):
        if not self.valid:
            return
        prediction, radius, indexes = self.clf.get_data(xc, yc)
        theta = np.linspace(0, 2 * np.pi, 60)
        with self.ln_circle.hold_sync():
            self.ln_circle.x = xc + radius * np.cos(theta)
            self.ln_circle.y = yc + radius * np.sin(theta)
            self.ln_circle.color = [prediction[0]]
        self.sr_points.selected = indexes

    def ck_bt_reset(self, bt_reset):
        if self.source_df == 'internal':
            self.init_df(None, '', '', '')
        elif self.source_df == 'external':
            self.df = self.df_original.copy()
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_sr_center()
        self.update_axes(force=True)

    def cg_sl_k(self, change):
        xc, yc = self.sr_center.x[0], self.sr_center.y[0]
        self.reset_clf()
        self.update_sr_center(xc=xc, yc=yc)

    def cg_sl_new_point_class(self, change):
        # fix for bqplot not coloring points immediately
        self.sr_points.color = self.sr_points.color[:-1]
        self.preplace_color_for_new_point()

    def dr_sr_center(self, sr_center, event):
        if event.get('event') == 'drag_start':
            self.set_animations(False, action='store')
        if event.get('event') == 'drag':
            xc, yc = event['point']['x'], event['point']['y']
            self.update_ln_circle(xc, yc)
        if event.get('event') == 'drag_end':
            self.set_animations(True, action='restore')
            self.update_axes()

    def dr_sr_points(self, sr_points, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_points = False
        if event.get('event') == 'drag':
            self.dragging_sr_points = True
        if event.get('event') == 'drag_end':
            if self.dragging_sr_points:
                point = event['point']['x'], event['point']['y']
                index = self.df.index[event['index']]
                self.df.loc[index, ['f1', 'f2']] = point
                self.update_after_df()
                self.reset_clf()
                self.update_axes()
                self.update_ln_circle_same_center()
            else:
                index = event['index']
                self.df = self.df.drop(self.df.index[index])
                self.update_after_df()
                self.reset_clf()
                self.set_animations(False, action='store')
                self.update_sr_points()
                self.set_animations(True, action='restore')
                self.update_axes()
                self.update_ln_circle_same_center()

    def ck_sr_points_background(self, sr_points, event):
        sr = self.sr_points
        new_point = sr.x[-1], sr.y[-1], sr.color[-1]
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.update_ln_circle_same_center()

    def set_info(self, switch):
        for mark in self.fig.marks:
            mark.display_legend = switch
        self.sr_points.tooltip = self.tt_point_loc if switch else None

    def preplace_color_for_new_point(self):
        # fix for bqplot not coloring points immediately
        color_value = self.sl_new_point_class.value
        self.sr_points.color = np.append(self.sr_points.color, color_value)


class DecisionTree:
    def __init__(self, df, c):
        self.clf = tree.DecisionTreeClassifier()
        self.clf.fit(df.loc[:, df.columns != c], df[c])

    def get_max_depth(self):
        return self.clf.get_depth()

    def go_left(self, node):
        return self.clf.tree_.children_left[node]

    def go_right(self, node):
        return self.clf.tree_.children_right[node]

    def get_children_of_node(self, node=0):
        children_left = self.clf.tree_.children_left
        children_right = self.clf.tree_.children_right
        queue, node_ids = [node], []
        while len(queue) > 0:
            node_id = queue.pop()
            node_ids.append(node_id)
            left_id, right_id = children_left[node_id], children_right[node_id]
            if left_id == right_id:
                continue
            queue.append(left_id)
            queue.append(right_id)
        return node_ids

    def get_path_from_root_to_node(self, node=0):
        children_left = self.clf.tree_.children_left
        children_right = self.clf.tree_.children_right
        queue, path = [0], []
        while len(queue) > 0:
            node_id = queue.pop()
            path.append(node_id)
            if node_id == node:
                return path
            left_id, right_id = children_left[node_id], children_right[node_id]
            if left_id == right_id:
                continue
            queue.append(left_id if node < right_id else right_id)
        return []

    def generate_graph(self, feature_names, class_names):
        return tree.export_graphviz(
            self.clf, node_ids=True, filled=True, feature_names=feature_names,
            class_names=class_names)


class ValetudoDecisionTree(ValetudoBase):
    """Interactive application for decision tree.

    Study the basics of the decision tree classifier.
    Explore the tree to learn how this algorithm predicts.

    Parameters
    ----------
    df: pandas.DataFrame
        A pandas dataframe containing the data for the interactive
        application. Only the columns with numeric values will be utilized.
        Columns with categorical values should be properly encoded.
        Rows with not-a-numbers will be removed.
    c: str
        The column name of the classes that would be used in the interactive
        application. The values will be label encoded.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, the dataframe is of
        insufficient length, no numeric columns to work with, or the number
        of classes is bellow 2.
    """

    def __init__(self, df: pd.DataFrame = None, c: str = ''):
        super().__init__()
        self.node = 0
        self.path = [self.node]
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, c)
        self.reset_clf()
        self.update_gr_tree()
        self.update_axes(force=True)
        self.update_ln_path()
        self.tb_autorange.value = False

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>Decision Tree</h1>')
        self.bt_root = ipw.Button(description='Back to Root')
        self.bt_go_up = ipw.Button(description='Go Up')
        self.bt_go_left = ipw.Button(description='Go Left')
        self.bt_go_right = ipw.Button(description='Go Right')
        self.app_controls = [
            self.bt_root, self.bt_go_up, self.bt_go_left, self.bt_go_right]

    def add_marks(self):
        fields = [
            'node_id', 'decision', 'impurity', 'samples', 'value',
            'prediction']
        self.tt_node = bq.Tooltip(fields=fields)
        self.ln_path = bq.Lines(
            scales=self.scs_x_y, stroke_width=20, opacities=[0.2],
            labels='Decision Path')
        self.gr_tree = bq.Graph(
            scales=self.scs_x_y, link_type='line', enable_hover=True)
        self.fig.marks = [self.ln_path, self.gr_tree]
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_root.on_click(self.ck_bt_root)
        self.bt_go_up.on_click(self.ck_bt_go_up)
        self.bt_go_left.on_click(self.ck_bt_go_left)
        self.bt_go_right.on_click(self.ck_bt_go_right)
        self.gr_tree.on_element_click(self.ck_gr_tree_element)

    def init_df(self, df, c):
        self.check_arg_df(df)
        self.check_arg_col(df, c)
        self.df = df.dropna()
        self.check_arg_df_min_rows(self.df, 2)
        col_c = self.df[c].copy()
        self.df = self.df.loc[:, self.df.columns != c]
        self.df = self.df.select_dtypes(include=np.number)
        if self.df.shape[1] == 0:
            raise ValueError('There are no numeric columns to work with.')
        self.features_names = self.df.columns
        self.c = c
        self.le = preprocessing.LabelEncoder()
        self.df[c] = self.le.fit_transform(col_c)
        if self.le.classes_.size < 2:
            raise ValueError('Not enough classes. At least 2 are required.')
        self.update_ht_classes()

    def reset_clf(self):
        self.clf = DecisionTree(self.df, self.c)

    def update_gr_tree(self):
        classes = list(map(str, self.le.classes_))
        dot = self.clf.generate_graph(self.features_names, classes)
        data = self.dot_to_json(dot)
        nodes = [self.parse_node(node) for node in data['objects']]
        colors = [node['fillcolor'] for node in data['objects']]
        links = [
            {'source': edge['tail'], 'target': edge['head'], 'value': 0}
            for edge in data['edges']]
        x = [float(node['pos'].split(',')[0]) for node in data['objects']]
        y = [float(node['pos'].split(',')[1]) for node in data['objects']]
        directed = data['directed']
        with self.gr_tree.hold_sync():
            self.gr_tree.node_data = nodes
            self.gr_tree.link_data = links
            self.gr_tree.x, self.gr_tree.y = x, y
            self.gr_tree.directed = directed
            self.gr_tree.colors = colors

    def parse_node(self, node):
        lines = node['label'].split('\\n')
        its_leaf = len(lines) == 5  # 6 for decision node
        node_id = int(lines[0].split('#')[-1])
        decision = lines[1] if not its_leaf else None
        impurity = lines[-4]
        samples = int(lines[-3].split(' = ')[-1])
        value = lines[-2].split(' = ')[-1]
        prediction = lines[-1].split(' = ')[-1]
        shape = 'rect' if its_leaf else 'circle'
        return {
            'label': node['name'],
            'node_id': node_id,
            'decision': decision,
            'impurity': impurity,
            'samples': samples,
            'value': value,
            'shape': shape,
            'prediction': prediction}

    def update_axes(self, force=False):
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits(*limits)
        self.set_limits(*limits)

    def get_data_limits(self):
        children = self.clf.get_children_of_node(self.node)
        if len(children) == 1 and len(self.path) >= 2:
            children = self.clf.get_children_of_node(self.path[-2])
        x = [self.gr_tree.x[child] for child in children]
        y = [self.gr_tree.y[child] for child in children]
        return min(x), max(x), min(y), max(y)

    def update_ln_path(self):
        x = [self.gr_tree.x[node] for node in self.path]
        y = [self.gr_tree.y[node] for node in self.path]
        with self.ln_path.hold_sync():
            self.ln_path.x, self.ln_path.y = x, y

    def ck_bt_root(self, bt_root):
        self.node = 0
        self.path = [self.node]
        its_in_x = self.sc_x.min < self.gr_tree.x[self.node] < self.sc_x.max
        its_in_y = self.sc_y.min < self.gr_tree.y[self.node] < self.sc_y.max
        self.update_axes(force=not its_in_x or not its_in_y)
        self.update_ln_path()

    def ck_bt_go_up(self, bt_go_up):
        if len(self.path) < 2:
            return
        self.path.pop()
        self.node = self.path[-1]
        its_in_x = self.sc_x.min < self.gr_tree.x[self.node] < self.sc_x.max
        its_in_y = self.sc_y.min < self.gr_tree.y[self.node] < self.sc_y.max
        self.update_axes(force=not its_in_x or not its_in_y)
        self.update_ln_path()

    def ck_bt_go_left(self, bt_go_left):
        new_root = self.clf.go_left(self.node)
        if new_root == -1:
            return
        self.node = new_root
        self.path.append(self.node)
        self.update_axes()
        self.update_ln_path()

    def ck_bt_go_right(self, bt_go_right):
        new_root = self.clf.go_right(self.node)
        if new_root == -1:
            return
        self.node = new_root
        self.path.append(self.node)
        self.update_axes()
        self.update_ln_path()

    def ck_gr_tree_element(self, gr_tree, event):
        self.node = event['data']['node_id']
        self.path = self.clf.get_path_from_root_to_node(self.node)
        self.update_axes()
        self.update_ln_path()

    def dot_to_json(self, graph):
        path = gv.Source(graph).render('.temp_graph', format='json')
        data = json.load(open(path, 'r'))
        for file in ['.temp_graph', '.temp_graph.json', 'noname.gv.xdot']:
            if os.path.exists(file):
                os.remove(file)
        return data

    def update_ht_classes(self):
        one_class = '{}. {}<br>'
        classes_text = '<h2>Classes</h2>'
        for index, class_name in enumerate(self.le.classes_):
            classes_text += one_class.format(index, class_name)
        self.ht_classes.value = classes_text

    def set_info(self, switch):
        self.gr_tree.tooltip = self.tt_node if switch else None


class LinearRegression:
    def __init__(self, df):
        self.reg = linear_model.LinearRegression()
        self.reg.fit(df[['x']], df['y'])

    def coeffs(self):
        return self.reg.intercept_, self.reg.coef_[0]


class ValetudoLinearRegression(ValetudoBase):
    """Interactive application for linear regression.

    Study the basics of linear regression.
    You can add, move, or delete points interactivels to recalculate the
    regression line.

    Parameters
    ----------
    df: pandas.DataFrame, optional
        A pandas dataframe containing the data for the interactive
        application. If this argument is omitted, a random set of points
        is generated instead. The columns to be utilized should be provided
        in the appropriate arguments. Rows with not-a-numbers will be removed.
    x, y: str, optional
        If a dataframe is provided via `df`, `x` and `y` are the column
        names of the features that would be used in the interactive
        application. The data in these columns has to be numeric.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, or the dataframe is of
        insufficient length.
    """

    def __init__(self, df: pd.DataFrame = None, x: str = '', y: str = ''):
        super().__init__()
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, x, y)
        self.reset_reg()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_ln_regression()

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>Linear Regression</h1>')
        self.bt_reset = ipw.Button(description='Reset', button_style='danger')
        self.hm_equation = ipw.HTMLMath(
            value='<h2>Regression Line</h2>$y=bx+a$')
        self.ht_error = ipw.HTML()
        self.app_controls = [self.bt_reset, self.hm_equation, self.ht_error]

    def add_marks(self):
        self.ln_error = bq.Lines(
            x=[], y=[], scales=self.scs_x_y_c, color=[2], labels='Error')
        self.ln_regression = bq.Lines(
            x=[], y=[], scales=self.scs_x_y_c, color=[1],
            labels='Regression Line')
        self.sr_points = bq.Scatter(
            x=[], y=[], enable_move=True, scales=self.scs_x_y_c,
            tooltip=self.tt_point_loc, color=[0], labels='Points',
            interactions={'click': 'add', 'hover': 'tooltip'})
        self.fig.marks = [self.ln_error, self.ln_regression, self.sr_points]
        self.sc_c.domain = list(range(3))
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_reset.on_click(self.ck_bt_reset)
        self.sr_points.on_hover(self.hv_sr_points)
        self.sr_points.on_drag_start(self.dr_sr_points)
        self.sr_points.on_drag(self.dr_sr_points)
        self.sr_points.on_drag_end(self.dr_sr_points)
        self.sr_points.on_background_click(self.ck_sr_points_background)

    def init_df(self, df, x, y):
        column_names = ['x', 'y']
        if df is None:
            self.x, self.y = column_names
            data = self.rng.standard_normal(size=(3, 2))
            self.df = pd.DataFrame(data, columns=column_names)
            self.source_df = 'internal'
            self.df_original = self.df.copy()
            self.update_after_df()
            return
        self.check_arg_df(df)
        [self.check_arg_col_numeric(df, col) for col in [x, y]]
        self.df = df[[x, y]].copy().dropna()
        self.check_arg_df_min_rows(self.df, 2)
        self.df.columns = column_names
        self.x, self.y = x, y
        self.source_df = 'external'
        self.df_original = self.df.copy()
        self.update_after_df()

    def update_after_df(self):
        self.validate_df()
        self.ax_x.label, self.ax_y.label = self.x, self.y
        self.tt_point_loc.labels = [self.x, self.y]

    def validate_df(self):
        self.valid, msg = self.validate_df_runtime()
        error = MSG_ERROR.replace('MESSAGE', msg)
        self.ht_error.value = error if not self.valid else ''

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            msg = 'Not enough data points.'
            msg += ' At least 2 are required. Place some points.'
            return False, msg
        return True, ''

    def reset_reg(self):
        if not self.valid:
            return
        self.reg = LinearRegression(self.df)

    def update_sr_points(self):
        with self.sr_points.hold_sync():
            self.sr_points.x = self.df.x.astype(float)
            self.sr_points.y = self.df.y.astype(float)

    def update_axes(self, force=False):
        if not self.valid:
            return
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits(*limits)
        self.set_limits(*limits)

    def get_data_limits(self):
        x_min, x_max = self.df.x.min(), self.df.x.max()
        y_min, y_max = self.df.y.min(), self.df.y.max()
        return x_min, x_max, y_min, y_max

    def project_ln_regression(self):
        if not self.valid:
            return
        intercept, slope = self.reg.coeffs()
        if abs(np.tan(slope)) <= 1:
            x = np.linspace(self.sc_x.min, self.sc_x.max, 2)
            y = intercept + slope * x
        else:
            intercept, slope = -intercept / slope, 1 / slope
            y = np.linspace(self.sc_y.min, self.sc_y.max, 2)
            x = intercept + slope * y
        with self.ln_regression.hold_sync():
            self.ln_regression.x, self.ln_regression.y = x, y
        self.update_hm_equation(intercept, slope)

    def update_hm_equation(self, intercept, slope):
        text = "<h2>Regression Line</h2>"
        sign = '+' if intercept >= 0 else '-'
        text += f'$y={slope:.2f}x{sign}{abs(intercept):.2f}$'
        self.hm_equation.value = text

    def ck_bt_reset(self, bt_reset):
        self.remove_ln_error()
        if self.source_df == 'internal':
            self.init_df(None, '', '')
        elif self.source_df == 'external':
            self.df = self.df_original.copy()
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_ln_regression()

    def hv_sr_points(self, sr_points, event):
        if not self.valid:
            return
        point = event['data']['x'], event['data']['y']
        self.update_ln_error(*point)

    def dr_sr_points(self, sr_points, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_points = False
        if event.get('event') == 'drag':
            self.dragging_sr_points = True
        if event.get('event') == 'drag_end':
            if self.dragging_sr_points:
                point = event['point']['x'], event['point']['y']
                self.df.loc[self.df.index[event['index']], ['x', 'y']] = point
                self.update_after_df()
                self.reset_reg()
                self.update_axes()
                self.project_ln_regression()
                self.update_ln_error(*point)
            else:
                index = event['index']
                self.df = self.df.drop(self.df.index[index])
                self.update_after_df()
                self.reset_reg()
                self.set_animations(False, action='store')
                self.update_sr_points()
                self.set_animations(True, action='restore')
                self.update_axes()
                self.project_ln_regression()
                self.remove_ln_error()

    def ck_sr_points_background(self, sr_points, event):
        sr = self.sr_points
        new_point = sr.x[-1], sr.y[-1]
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_regression()
        self.update_ln_error(*new_point)

    def remove_ln_error(self):
        with self.ln_error.hold_sync():
            self.ln_error.x = self.ln_error.y = []

    def update_ln_error(self, x1, y1):
        if not self.valid:
            return
        intercept, slope = self.reg.coeffs()
        x2, y2 = x1, intercept + slope * x1
        with self.ln_error.hold_sync():
            self.ln_error.x, self.ln_error.y = [x1, x2], [y1, y2]

    def set_info(self, switch):
        for mark in self.fig.marks:
            mark.display_legend = switch
        self.sr_points.tooltip = self.tt_point_loc if switch else None


class LogisticRegression:
    def __init__(self, df):
        self.reg = linear_model.LogisticRegression()
        self.reg.fit(df[['x']], df['y'])

    def coeffs(self):
        return self.reg.intercept_[0], self.reg.coef_[0][0]


class ValetudoLogisticRegression(ValetudoBase):
    """Interactive application for logistic regression.

    Study the basics of logistic regression.
    You can add, move, or delete points interactivels to recalculate the
    logistic function.

    Parameters
    ----------
    df: pandas.DataFrame, optional
        A pandas dataframe containing the data for the interactive
        application. If this argument is omitted, a random set of points
        is generated instead. The columns to be utilized should be provided
        in the appropriate arguments. Rows with not-a-numbers will be removed.
    x: str, optional
        If a dataframe is provided via `df`, `x` is the column name of the
        features that would be used in the interactive application. The data
        in the column has to be numeric.
    y: str, optional
        If a dataframe is provided via `df`, `y` is the column name of the
        classes that would be used in the interactive application. The values
        will be label encoded.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, or the dataframe is of
        insufficient length.
    """

    def __init__(self, df: pd.DataFrame = None, x: str = '', y: str = ''):
        super().__init__()
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, x, y)
        self.reset_reg()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_ln_logistic()

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>Logistic Regression</h1>')
        self.bt_reset = ipw.Button(description='Reset', button_style='danger')
        text = '<h2>Logistic Function</h2>$\\large{y=\\frac{1}{1+e^{-x}}}$'
        self.hm_equation = ipw.HTMLMath(value=text)
        self.ht_error = ipw.HTML()
        self.app_controls = [self.bt_reset, self.hm_equation, self.ht_error]

    def add_marks(self):
        self.ln_logistic = bq.Lines(
            x=[], y=[], scales=self.scs_x_y_c, color=[2],
            labels='Logistic Function')
        self.sr_points = bq.Scatter(
            x=[], y=[], enable_move=True, restrict_x=True,
            tooltip=self.tt_point_loc, scales=self.scs_x_y_c,
            interactions={'click': 'add', 'hover': 'tooltip'},
            labels='Points')
        self.fig.marks = [self.ln_logistic, self.sr_points]
        self.sc_c.domain = list(range(3))
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_reset.on_click(self.ck_bt_reset)
        self.sr_points.on_drag_start(self.dr_sr_points)
        self.sr_points.on_drag(self.dr_sr_points)
        self.sr_points.on_drag_end(self.dr_sr_points)
        self.sr_points.on_background_click(self.ck_sr_points_background)

    def init_df(self, df, x, y):
        column_names = ['x', 'y']
        if df is None:
            self.x, self.y = column_names
            data = np.sort(self.rng.standard_normal(size=3))
            self.df = pd.DataFrame(data, columns=['x'])
            self.df['y'] = [0, 0, 1]
            self.source_df = 'internal'
            self.df_original = self.df.copy()
            self.update_after_df()
            return
        self.check_arg_df(df)
        self.check_arg_col_numeric(df, x)
        self.check_arg_col(df, y)
        self.df = df[[x, y]].copy().dropna()
        self.check_arg_df_min_rows(self.df, 2)
        self.df.columns = column_names
        self.x, self.y = x, y
        self.le = preprocessing.LabelEncoder()
        self.df.y = self.le.fit_transform(self.df.y)
        if self.le.classes_.size != 2:
            msg = 'Invalid number of classes. Exactly 2 are required.'
            raise ValueError(msg)
        self.update_ht_classes()
        self.source_df = 'external'
        self.df_original = self.df.copy()
        self.update_after_df()

    def update_after_df(self):
        self.validate_df()
        self.ax_x.label, self.ax_y.label = self.x, self.y
        self.tt_point_loc.labels = [self.x, self.y]

    def validate_df(self):
        self.valid, msg = self.validate_df_runtime()
        error = MSG_ERROR.replace('MESSAGE', msg)
        self.ht_error.value = error if not self.valid else ''

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            msg = 'Not enough data points.'
            msg += ' At least 2 are required. Place some points.'
            return False, msg
        if self.df.y.unique().size != 2:
            msg = 'Invalid number of classes.'
            msg += ' Exactly 2 are required. Place some points.'
            return False, msg
        return True, ''

    def reset_reg(self):
        if not self.valid:
            return
        self.reg = LogisticRegression(self.df)

    def update_sr_points(self):
        with self.sr_points.hold_sync():
            self.sr_points.x = self.df.x.astype(float)
            self.sr_points.y = self.df.y.astype(float)
            self.sr_points.color = self.df.y

    def update_axes(self, force=False):
        if not self.valid:
            return
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits(*limits)
        self.set_limits(*limits)

    def get_data_limits(self):
        intercept, slope = self.reg.coeffs()
        yl = np.log(99)  # zoom on 99 % of the logistic curve
        xl = (np.array([yl, -yl]) - intercept) / slope
        x_min = min(self.df.x.min(), min(xl))
        x_max = max(self.df.x.max(), max(xl))
        y_min, y_max = 0, 1
        return x_min, x_max, y_min, y_max

    def project_ln_logistic(self):
        if not self.valid:
            return
        intercept, slope = self.reg.coeffs()
        x = np.linspace(self.sc_x.min, self.sc_x.max, 120)
        y = special.expit(intercept + slope * x).ravel()
        with self.ln_logistic.hold_sync():
            self.ln_logistic.x, self.ln_logistic.y = x, y
        self.update_hm_equation(intercept, slope)

    def update_hm_equation(self, intercept, slope):
        text = "<h2>Logistic Function</h2>"
        sign = '+' if intercept >= 0 else '-'
        regression_line = f'{slope:.2f}x{sign}{abs(intercept):.2f}'
        text += '$\\large{y=\\frac{1}{1+e^{-(' + regression_line + ')}}}$'
        self.hm_equation.value = text

    def ck_bt_reset(self, bt_reset):
        if self.source_df == 'internal':
            self.init_df(None, '', '')
        elif self.source_df == 'external':
            self.df = self.df_original.copy()
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_ln_logistic()

    def dr_sr_points(self, sr_points, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_points = False
        if event.get('event') == 'drag':
            self.dragging_sr_points = True
        if event.get('event') == 'drag_end':
            if self.dragging_sr_points:
                point = event['point']['x'], round(event['point']['y'])
                index = self.df.index[event['index']]
                self.df.loc[index, ['x', 'y']] = point
                self.update_after_df()
                self.reset_reg()
                self.update_sr_points()
                self.update_axes()
                self.project_ln_logistic()
            else:
                index = event['index']
                self.df = self.df.drop(self.df.index[index])
                self.update_after_df()
                self.reset_reg()
                self.set_animations(False, action='store')
                self.update_sr_points()
                self.set_animations(True, action='restore')
                self.update_axes()
                self.project_ln_logistic()

    def ck_sr_points_background(self, sr_points, event):
        sr = self.sr_points
        new_point = sr.x[-1], 1 if sr.y[-1] > 0.5 else 0
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_logistic()

    def set_info(self, switch):
        for mark in self.fig.marks:
            mark.display_legend = switch
        self.sr_points.tooltip = self.tt_point_loc if switch else None


class Perceptron:
    def __init__(self, df, eta):
        self.clf = linear_model.Perceptron(eta0=eta)
        self.clf.partial_fit(df[['f1', 'f2']], df.c, classes=df.c.unique())

    def fit(self, df):
        self.clf.fit(df[['f1', 'f2']], df.c)

    def partial_fit(self, df):
        self.clf.partial_fit(df[['f1', 'f2']], df.c)

    def set_params(self, eta):
        self.clf.set_params(eta0=eta)

    def coeffs(self, n_class=0):
        n_class = 0 if self.clf.classes_.size == 2 else n_class
        return self.clf.intercept_[n_class], self.clf.coef_[n_class]


class ValetudoPerceptron(ValetudoBase):
    """Interactive application for the perceptron.

    Study the basics of the perceptron.
    Go step by step to demonstrate how the perceptron classifies the data. You
    can select point for a partial training, with the option of controling the
    learning rate. You can add, move, or delete points interactively.

    Parameters
    ----------
    df: pandas.DataFrame, optional
        A pandas dataframe containing the data for the interactive
        application. If this argument is omitted, a random set of points
        is generated instead. The columns to be utilized should be provided
        in the appropriate arguments. Rows with not-a-numbers will be removed.
    f1, f2: str, optional
        If a dataframe is provided via `df`, `f1` and `f2` are the column
        names of the features that would be used in the interactive
        application. The data in these columns has to be numeric.
    c: str, optional
        If a dataframe is provided via `df`, `c` is the column name of the
        classes that would be used in the interactive application. The values
        will be label encoded.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, or the dataframe is of
        insufficient length.
    """

    def __init__(
        self, df: pd.DataFrame = None,
        f1: str = '', f2: str = '', c: str = ''
    ):
        super().__init__()
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, f1, f2, c)
        self.reset_clf()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_ln_separation()

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>Perceptron</h1>')
        self.bt_reset = ipw.Button(description='Reset', button_style='danger')
        self.bt_reset_clf = ipw.Button(description='Reset Classifier')
        self.tbs_mode_click = ipw.ToggleButtons(
            options=['Add or Delete', 'Select'], value='Add or Delete')
        self.sl_new_point_class = ipw.IntSlider(
            value=0, min=0, max=0, description='New Point Class:',
            style={'description_width': 'initial'})
        self.sl_predict_for = ipw.IntSlider(
            value=0, min=0, max=0, description='Predict for:')
        self.sl_eta = ipw.FloatSlider(
            value=0.5, min=0.1, max=1, description='η:')
        self.bt_run_at_once = ipw.Button(description='Run At Once')
        self.bt_step = ipw.Button(description='Step', button_style='primary')
        self.ht_error = ipw.HTML()
        self.ou_classes = ipw.Output()
        self.app_controls = [
            self.bt_reset, self.bt_reset_clf, self.tbs_mode_click,
            self.sl_new_point_class, self.sl_predict_for, self.sl_eta,
            self.bt_run_at_once, self.bt_step, self.ht_error, self.ou_classes]

    def add_marks(self):
        self.ln_separation = bq.Lines(
            x=[], y=[], scales=self.scs_x_y_c, labels='Line of Separation')
        self.sr_points = bq.Scatter(
            x=[], y=[], scales=self.scs_x_y_c, tooltip=self.tt_point_loc,
            interactions={'click': 'add', 'hover': 'tooltip'},
            enable_move=True, labels='Points',
            unselected_style={'opacity': 0.5})
        self.fig.marks = [self.ln_separation, self.sr_points]
        self.fig.axes = self.axs_x_y_c
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_reset.on_click(self.ck_bt_reset)
        self.bt_reset_clf.on_click(self.ck_bt_reset_clf)
        self.tbs_mode_click.observe(self.cg_tbs_mode_click, 'value')
        self.sl_new_point_class.observe(self.cg_sl_new_point_class, 'value')
        self.sl_predict_for.observe(self.cg_sl_predict_for, 'value')
        self.sl_eta.observe(self.cg_sl_eta, 'value')
        self.bt_run_at_once.on_click(self.ck_bt_run_at_once)
        self.bt_step.on_click(self.ck_bt_step)
        self.sr_points.on_drag_start(self.dr_sr_points)
        self.sr_points.on_drag(self.dr_sr_points)
        self.sr_points.on_drag_end(self.dr_sr_points)
        self.sr_points.on_background_click(self.ck_sr_points_background)
        for wd in [self.sl_new_point_class, self.sl_predict_for]:
            ipw.dlink(
                (wd, 'value'), (wd.style, 'handle_color'),
                transform=self.get_color_for_n)

    def init_df(self, df, f1, f2, c):
        column_names = ['f1', 'f2', 'c']
        if df is None:
            self.f1, self.f2, self.c = column_names
            data = self.rng.standard_normal(size=(3, 2))
            self.df = pd.DataFrame(data, columns=column_names[:2])
            self.df[self.c] = [0, 0, 1]
            self.source_df = 'internal'
            self.df_original = self.df.copy()
            self.update_after_df()
            return
        self.check_arg_df(df)
        [self.check_arg_col_numeric(df, col) for col in [f1, f2]]
        self.check_arg_col(df, c)
        self.df = df[[f1, f2, c]].copy().dropna()
        self.check_arg_df_min_rows(self.df, 2)
        self.df.columns = column_names
        self.f1, self.f2, self.c = f1, f2, c
        self.le = preprocessing.LabelEncoder()
        self.df.c = self.le.fit_transform(self.df.c)
        self.update_ht_classes()
        self.source_df = 'external'
        self.df_original = self.df.copy()
        self.update_after_df()

    def update_after_df(self):
        self.validate_df()
        self.ax_x.label, self.ax_y.label = self.f1, self.f2
        self.tt_point_loc.labels = [self.f1, self.f2]
        n_classes_max = max(self.df.c, default=-1)
        self.sl_new_point_class.max = n_classes_max + 1
        self.sl_predict_for.max = max(0, n_classes_max)
        self.sc_c.domain = list(range(n_classes_max + 1))

    def validate_df(self):
        self.valid, msg = self.validate_df_runtime()
        error = MSG_ERROR.replace('MESSAGE', msg)
        self.ht_error.value = error if not self.valid else ''

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            msg = 'Not enough data points.'
            msg += ' At least 2 are required. Place some points.'
            return False, msg
        if self.df.c.unique().size < 2:
            msg = 'Invalid number of classes.'
            msg += ' At least 2 are required. Place some points.'
            return False, msg
        return True, ''

    def reset_clf(self):
        if not self.valid:
            return
        self.clf = Perceptron(self.df, eta=self.sl_eta.value)

    def update_sr_points(self):
        with self.sr_points.hold_sync():
            self.sr_points.x = self.df.f1.astype(float)
            self.sr_points.y = self.df.f2.astype(float)
            self.sr_points.color = self.df.c
        self.preplace_color_for_new_point()

    def update_axes(self, force=False):
        if not self.valid:
            return
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits(*limits)
        limits = self.expand_limits_for_ln_separation(*limits)
        self.set_limits(*limits)

    def get_data_limits(self):
        x_min, x_max = self.df.f1.min(), self.df.f1.max()
        y_min, y_max = self.df.f2.min(), self.df.f2.max()
        return x_min, x_max, y_min, y_max

    def expand_limits_for_ln_separation(self, x_min, x_max, y_min, y_max):
        if not self.valid:
            return x_min, x_max, y_min, y_max
        bias, weights = self.clf.coeffs(self.sl_predict_for.value)
        intercept, slope = -bias / weights[1], -weights[0] / weights[1]
        if abs(np.tan(slope)) <= 1:
            x = np.linspace(x_min, x_max, 2)
            y = intercept + slope * x
            y_min, y_max = min(y_min, min(y)), max(y_max, max(y))
        else:
            intercept, slope = -bias / weights[0], -weights[1] / weights[0]
            y = np.linspace(y_min, y_max, 2)
            x = intercept + slope * y
            x_min, x_max = min(x_min, min(x)), max(x_max, max(x))
        return x_min, x_max, y_min, y_max

    def project_ln_separation(self):
        if not self.valid:
            return
        bias, weights = self.clf.coeffs(self.sl_predict_for.value)
        intercept, slope = -bias / weights[1], -weights[0] / weights[1]
        if abs(np.tan(slope)) <= 1:
            x = np.linspace(self.sc_x.min, self.sc_x.max, 2)
            y = intercept + slope * x
        else:
            intercept, slope = -bias / weights[0], -weights[1] / weights[0]
            y = np.linspace(self.sc_y.min, self.sc_y.max, 2)
            x = intercept + slope * y
        with self.ln_separation.hold_sync():
            self.ln_separation.x, self.ln_separation.y = x, y

    def ck_bt_reset(self, bt_reset):
        self.sr_points.selected = None
        if self.source_df == 'internal':
            self.init_df(None, '', '', '')
        elif self.source_df == 'external':
            self.df = self.df_original.copy()
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_ln_separation()

    def ck_bt_reset_clf(self, bt_reset_clf):
        if not self.valid:
            return
        self.reset_clf()
        self.update_axes()
        self.project_ln_separation()

    def cg_tbs_mode_click(self, change):
        its_mode_add_or_delete = change['new'] == 'Add or Delete'
        its_mode_select = change['new'] == 'Select'
        self.sr_points.on_background_click(
            self.ck_sr_points_background, remove=its_mode_select)
        action = 'select' if not its_mode_add_or_delete else 'add'
        # shouldn't really set hover in the next line, but it's a fix
        self.sr_points.interactions = {'click': action, 'hover': 'tooltip'}
        if its_mode_add_or_delete:
            self.sr_points.selected = None

    def cg_sl_new_point_class(self, change):
        # fix for bqplot not coloring points immediately
        color = self.get_color_for_n(change['new'])
        self.sl_new_point_class.style.handle_color = color
        self.sr_points.color = self.sr_points.color[:-1]
        self.preplace_color_for_new_point()

    def cg_sl_predict_for(self, change):
        self.ln_separation.color = [change['new']]

    def cg_sl_eta(self, change):
        self.clf.set_params(eta=change['new'])

    def ck_bt_run_at_once(self, bt_run_at_once):
        if not self.valid:
            return
        self.clf.fit(self.df)
        self.update_axes()
        self.project_ln_separation()

    def ck_bt_step(self, bt_step):
        if not self.valid:
            return
        df = self.df
        if self.sr_points.selected is not None:
            df = self.df.iloc[self.sr_points.selected]
        self.clf.partial_fit(df)
        self.update_axes()
        self.project_ln_separation()

    def dr_sr_points(self, sr_points, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_points = False
        if event.get('event') == 'drag':
            self.dragging_sr_points = True
        if event.get('event') == 'drag_end':
            if self.dragging_sr_points:
                point = event['point']['x'], event['point']['y']
                index = self.df.index[event['index']]
                self.df.loc[index, ['f1', 'f2']] = point
                self.update_axes()
                self.project_ln_separation()
            else:
                if self.tbs_mode_click.value != 'Add or Delete':
                    return
                index = event['index']
                its_a_removal_of_a_class = list(self.df.c).count(index) == 1
                self.df = self.df.drop(self.df.index[index])
                self.update_after_df()
                if its_a_removal_of_a_class:
                    self.reset_clf()
                self.set_animations(False, action='store')
                self.update_sr_points()
                self.set_animations(True, action='restore')
                self.update_axes()
                self.project_ln_separation()

    def ck_sr_points_background(self, sr_points, event):
        sr = self.sr_points
        new_point = sr.x[-1], sr.y[-1], sr.color[-1]
        its_a_new_class = self.sr_points.color[-1] not in self.df.c.values
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        if its_a_new_class:
            self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_ln_separation()

    def set_info(self, switch):
        for mark in self.fig.marks:
            mark.display_legend = switch
        self.sr_points.tooltip = self.tt_point_loc if switch else None

    def preplace_color_for_new_point(self):
        # fix for bqplot not coloring points immediately
        color_value = self.sl_new_point_class.value
        self.sr_points.color = np.append(self.sr_points.color, color_value)


class KMeans:
    def __init__(self, df, n_centroids, n_steps):
        self.clf = cluster.KMeans(
            n_clusters=n_centroids, n_init=1, max_iter=n_steps)
        self.fit(df)

    def fit(self, df):
        self.clf.fit(df[['f1', 'f2']])

    def get_centroids(self):
        return self.clf.cluster_centers_.T

    def set_centroids(self, centroids):
        self.clf.cluster_centers_ = centroids.copy(order='C')

    def set_params(self, centroids, max_iter):
        self.clf.set_params(
            n_clusters=len(centroids), init=centroids, max_iter=max_iter)

    def predict(self, df):
        if df.shape[0] == 0:
            return []
        return self.clf.predict(df[['f1', 'f2']])


class ValetudoKMeans(ValetudoBase):
    """Interactive application for K-Means.

    Study the basics of the k-means algorithm.
    Go step by step to demonstrate how the centroids update their position.
    You can control the centroids. You can add, move, or delete points
    interactively.

    Parameters
    ----------
    df: pandas.DataFrame, optional
        A pandas dataframe containing the data for the interactive
        application. If this argument is omitted, a random set of points
        is generated instead. The columns to be utilized should be provided
        in the appropriate arguments. Rows with not-a-numbers will be removed.
    f1, f2: str, optional
        If a dataframe is provided via `df`, `f1` and `f2` are the column
        names of the features that would be used in the interactive
        application. The data in these columns has to be numeric.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, or the dataframe is of
        insufficient length.
    """

    def __init__(self, df: pd.DataFrame = None, f1: str = '', f2: str = ''):
        super().__init__()
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, f1, f2)
        self.reset_clf()
        self.init_centroids()
        self.update_sr_points()
        self.update_sr_centroids()
        self.update_axes(force=True)
        self.tb_scale_axes.value = True

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>K-Means</h1>')
        self.bt_reset = ipw.Button(description='Reset', button_style='danger')
        self.sl_k = ipw.IntSlider(
            value=3, min=1, max=self.max_n_classes, description='K:')
        self.bt_run_at_once = ipw.Button(description='Run At Once')
        self.bt_step = ipw.Button(description='Step', button_style='primary')
        self.ht_error = ipw.HTML()
        self.ou_classes = ipw.Output()
        self.app_controls = [
            self.bt_reset, self.sl_k, self.bt_run_at_once, self.bt_step,
            self.ht_error, self.ou_classes]

    def add_marks(self):
        self.sr_points = bq.Scatter(
            x=[], y=[], scales=self.scs_x_y_c, tooltip=self.tt_point_loc,
            interactions={'click': 'add', 'hover': 'tooltip'},
            enable_move=True, labels='Points')
        self.sr_centroids = bq.Scatter(
            x=[], y=[], scales=self.scs_x_y_c, tooltip=self.tt_point_loc,
            interactions={'hover': 'tooltip'}, labels='Centroids',
            enable_move=True, default_size=300, stroke='black')
        self.fig.marks = [self.sr_points, self.sr_centroids]
        self.fig.axes = self.axs_x_y_c
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_reset.on_click(self.ck_bt_reset)
        self.sl_k.observe(self.cg_sl_k, 'value')
        self.bt_run_at_once.on_click(self.ck_bt_run_at_once)
        self.bt_step.on_click(self.ck_bt_step)
        self.sr_points.on_drag_start(self.dr_sr_points)
        self.sr_points.on_drag(self.dr_sr_points)
        self.sr_points.on_drag_end(self.dr_sr_points)
        self.sr_points.on_background_click(self.ck_sr_points_background)
        self.sr_centroids.on_drag_start(self.dr_sr_centroids)
        self.sr_centroids.on_drag(self.dr_sr_centroids)
        self.sr_centroids.on_drag_end(self.dr_sr_centroids)

    def init_df(self, df, f1, f2):
        column_names = ['f1', 'f2']
        if df is None:
            self.f1, self.f2 = column_names
            data = self.rng.standard_normal(size=(5, 2))
            self.df = pd.DataFrame(data, columns=column_names)
            self.source_df = 'internal'
            self.df_original = self.df.copy()
            self.update_after_df()
            return
        self.check_arg_df(df)
        [self.check_arg_col_numeric(df, col) for col in [f1, f2]]
        self.df = df[[f1, f2]].copy().dropna()
        self.check_arg_df_min_rows(self.df, 1)
        self.df.columns = column_names
        self.f1, self.f2 = f1, f2
        self.source_df = 'external'
        self.df_original = self.df.copy()
        self.update_after_df()

    def update_after_df(self):
        self.validate_df()
        self.ax_x.label, self.ax_y.label = self.f1, self.f2
        self.sl_k.max = min(self.max_n_classes, max(1, self.df.shape[0]))
        self.sc_c.domain = list(np.arange(self.sl_k.value))
        self.tt_point_loc.labels = [self.f1, self.f2]

    def validate_df(self):
        self.valid, msg = self.validate_df_runtime()
        error = MSG_ERROR.replace('MESSAGE', msg)
        self.ht_error.value = error if not self.valid else ''

    def validate_df_runtime(self):
        if self.df.shape[0] < 1:
            msg = 'Not enough data points.'
            msg += ' At least 1 is required. Place some points.'
            return False, msg
        return True, ''

    def init_centroids(self):
        x_min, x_max, y_min, y_max = self.get_data_limits()
        x, y = self.rng.uniform(size=(2, self.sl_k.value))
        x = (x_max - x_min) * x + x_min
        y = (y_max - y_min) * y + y_min
        self.clf.set_centroids(np.vstack((x, y)).T)

    def reset_clf(self):
        if not self.valid:
            return
        self.clf = KMeans(self.df, self.sl_k.value, 1)

    def get_centroids(self):
        x, y = self.sr_centroids.x, self.sr_centroids.y
        return np.vstack((x, y)).T

    def update_sr_points(self):
        color = self.clf.predict(self.df)
        with self.sr_points.hold_sync():
            self.sr_points.x = self.df.f1.astype(float)
            self.sr_points.y = self.df.f2.astype(float)
            self.sr_points.color = color

    def update_sr_centroids(self):
        x, y = self.clf.get_centroids()
        with self.sr_centroids.hold_sync():
            self.sr_centroids.x, self.sr_centroids.y = x, y
            self.sr_centroids.color = np.arange(self.sl_k.value)

    def update_axes(self, force=False):
        if not self.valid:
            return
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits_for_centroids(*limits)
        limits = self.expand_limits(*limits)
        self.set_limits(*limits)

    def expand_limits_for_centroids(self, x_min, x_max, y_min, y_max):
        x_min_c, x_max_c = min(self.sr_centroids.x), max(self.sr_centroids.x)
        y_min_c, y_max_c = min(self.sr_centroids.y), max(self.sr_centroids.y)
        x_min, x_max = min(x_min, x_min_c), max(x_max, x_max_c)
        y_min, y_max = min(y_min, y_min_c), max(y_max, y_max_c)
        return x_min, x_max, y_min, y_max

    def get_data_limits(self):
        x_min, x_max = self.df.f1.min(), self.df.f1.max()
        y_min, y_max = self.df.f2.min(), self.df.f2.max()
        return x_min, x_max, y_min, y_max

    def ck_bt_reset(self, bt_reset):
        if self.source_df == 'internal':
            self.init_df(None, '', '')
        elif self.source_df == 'external':
            self.df = self.df_original.copy()
        self.update_after_df()
        self.update_sr_points()
        self.update_axes(force=True)
        self.wait_for_animation()
        self.init_centroids()
        self.update_sr_centroids()
        self.wait_for_animation()
        self.update_sr_points()
        self.update_axes(force=True)

    def cg_sl_k(self, change):
        self.init_centroids()
        self.update_sr_centroids()
        self.wait_for_animation()
        self.update_sr_points()
        self.update_axes()
        self.sc_c.domain = list(np.arange(self.sl_k.value))

    def ck_bt_run_at_once(self, bt_run_at_once):
        if not self.valid:
            return
        self.do_step(300)
        self.update_sr_centroids()
        self.wait_for_animation()
        self.update_sr_points()
        self.update_axes()

    def ck_bt_step(self, bt_step):
        if not self.valid:
            return
        self.do_step(1)
        self.update_sr_centroids()
        self.wait_for_animation()
        self.update_sr_points()
        self.update_axes()

    def do_step(self, n_steps):
        centroids = self.get_centroids()
        self.clf.set_params(centroids=centroids, max_iter=n_steps)
        self.clf.fit(self.df)

    def dr_sr_points(self, sr_points, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_points = False
        if event.get('event') == 'drag':
            self.dragging_sr_points = True
        if event.get('event') == 'drag_end':
            if self.dragging_sr_points:
                point = event['point']['x'], event['point']['y']
                index = self.df.index[event['index']]
                self.df.loc[index, ['f1', 'f2']] = point
                self.update_sr_points()
                self.update_axes()
            else:
                index = event['index']
                self.df = self.df.drop(self.df.index[index])
                self.update_after_df()
                self.set_animations(False, action='store')
                self.update_sr_points()
                self.set_animations(True, action='restore')
                self.update_axes()

    def ck_sr_points_background(self, sr_points, event):
        sr = self.sr_points
        new_point = sr.x[-1], sr.y[-1]
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.update_sr_points()
        self.update_axes()

    def dr_sr_centroids(self, sr_centroids, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_centroids = False
        elif event.get('event') == 'drag':
            self.dragging_sr_centroids = True
        elif event.get('event') == 'drag_end':
            if self.dragging_sr_centroids:
                centroids = self.get_centroids()
                point = event['point']['x'], event['point']['y']
                centroids[event['index']] = point  # drag bug
                self.clf.set_centroids(centroids)
                self.update_sr_points()
                self.update_axes()

    def set_info(self, switch):
        for mark in self.fig.marks:
            mark.display_legend = switch
        self.sr_points.tooltip = self.tt_point_loc if switch else None


class MLPRegressor:
    @ignore_warnings(category=ConvergenceWarning)
    def __init__(
            self,
            df,
            hiddens=(10,),
            alpha=0.01,
            learn=0.01,
            max_iter=50,
            ):
        self.reg = neural_network.MLPRegressor(
            hidden_layer_sizes=hiddens,
            alpha=alpha,
            learning_rate_init=learn,
            max_iter=max_iter,
            activation="tanh",
            solver="sgd",
            tol=1e-9,
            )
        self.reg.fit(df[['x']], df['y'])

    @ignore_warnings(category=ConvergenceWarning)
    def fit(self, df):
        self.reg.fit(df[['x']], df['y'])

    def predict(self, x):
        preds = self.reg.predict(x)
        return preds

    def set_params(
            self,
            hiddens=None,
            alpha=None,
            learn=None,
            max_iter=None,
            ):
        if hiddens is not None:
            self.reg.set_params(hidden_layer_sizes=hiddens)
        if alpha is not None:
            self.reg.set_params(alpha=alpha)
        if learn is not None:
            self.reg.set_params(learning_rate_init=learn)
        if max_iter is not None:
            self.reg.set_params(max_iter=max_iter)


class ValetudoMLPRegression(ValetudoBase):
    """Interactive application for neural network regression.

    Study the basics of neural network regression.
    You can add, move, or delete points interactivels to recalculate the
    regression line.
    You can change the learning rate and the number of hidden layers,
    regularization coefficient, and the maximum epochs in training.

    Parameters
    ----------
    df: pandas.DataFrame, optional
        A pandas dataframe containing the data for the interactive
        application. If this argument is omitted, a random set of points
        is generated instead. The columns to be utilized should be provided
        in the appropriate arguments. Rows with not-a-numbers will be removed.
    x, y: str, optional
        If a dataframe is provided via `df`, `x` and `y` are the column
        names of the features that would be used in the interactive
        application. The data in these columns has to be numeric.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, or the dataframe is of
        insufficient length.
    """

    def __init__(self, df: pd.DataFrame = None, x: str = '', y: str = ''):
        super().__init__()
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, x, y)
        self.reset_reg()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_mlp_regression()

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>Neural Network Regression</h1>')
        self.bt_reset = ipw.Button(description='Reset', button_style='danger')
        self.ht_error = ipw.HTML()
        self.architecture_title = ipw.HTML("<h3>Number of neurons in hidden layers</h3>")
        self.sl_h1 = ipw.IntSlider(
            value=5, min=1, max=10, description='HL 1:',
            style={'description_width': 'initial'})
        self.sl_h2 = ipw.IntSlider(
            value=0, min=0, max=10, description='HL 2:',
            style={'description_width': 'initial'})
        self.sl_h3 = ipw.IntSlider(
            value=0, min=0, max=10, description='HL 3:',
            style={'description_width': 'initial'})
        self.training_title = ipw.HTML("<h3>Training hyper-parameters</h3>")
        self.sl_max_iter = ipw.IntSlider(
            value=100, min=1, step=100, max=5000, description='Max iterations:',
            style={'description_width': 'initial'})
        self.sl_learn = ipw.FloatLogSlider(
            value=-2, min=-3, max=0.1, description='Learning rate:',
            style={'description_width': 'initial'})
        self.sl_alpha = ipw.FloatLogSlider(
            value=-2, min=-5, max=1, description='Regularization:',
            style={'description_width': 'initial'})
        self.app_controls = [
            self.bt_reset, self.ht_error,
            self.architecture_title, self.sl_h1, self.sl_h2, self.sl_h3,
            self.training_title, self.sl_max_iter, self.sl_learn, self.sl_alpha,
            ]

    def add_marks(self):
        self.mlp_regression = bq.Lines(
            x=[], y=[], scales=self.scs_x_y_c, color=[1],
            labels='NN Regression')
        self.sr_points = bq.Scatter(
            x=[], y=[], enable_move=True, scales=self.scs_x_y_c,
            tooltip=self.tt_point_loc, color=[0], labels='Points',
            interactions={'click': 'add', 'hover': 'tooltip'})
        self.fig.marks = [self.mlp_regression, self.sr_points]
        self.sc_c.domain = list(range(2))
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_reset.on_click(self.ck_bt_reset)
        self.sr_points.on_drag_start(self.dr_sr_points)
        self.sr_points.on_drag(self.dr_sr_points)
        self.sr_points.on_drag_end(self.dr_sr_points)
        self.sr_points.on_background_click(self.ck_sr_points_background)
        # slider controls
        self.sl_h1.observe(self.cg_sl_h, 'value')
        self.sl_h2.observe(self.cg_sl_h, 'value')
        self.sl_h3.observe(self.cg_sl_h, 'value')
        self.sl_max_iter.observe(self.cg_sl_max_iter, 'value')
        self.sl_learn.observe(self.cg_sl_learn, 'value')
        self.sl_alpha.observe(self.cg_sl_alpha, 'value')

    def cg_sl_h(self, change):
        hiddens = [self.sl_h1.value, self.sl_h2.value, self.sl_h3.value]
        hiddens = [h for h in hiddens if h > 0]
        self.reg.set_params(hiddens=hiddens)
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def cg_sl_max_iter(self, change):
        self.reg.set_params(max_iter=change["new"])
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def cg_sl_learn(self, change):
        self.reg.set_params(learn=change["new"])
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def cg_sl_alpha(self, change):
        self.reg.set_params(alpha=change["new"])
        self.reg.fit(self.df)
        self.project_mlp_regression()

    def init_df(self, df, x, y):
        column_names = ['x', 'y']
        if df is None:
            self.x, self.y = column_names
            xs = np.linspace(0, 2 * np.pi, 10)
            ys = np.sin(xs) + self.rng.standard_normal(size=xs.size) * 0.2
            self.df = pd.DataFrame({"x": xs, "y": ys})
            self.source_df = 'internal'
            self.df_original = self.df.copy()
            self.update_after_df()
            return
        self.check_arg_df(df)
        [self.check_arg_col_numeric(df, col) for col in [x, y]]
        self.df = df[[x, y]].copy().dropna()
        self.check_arg_df_min_rows(self.df, 2)
        self.df.columns = column_names
        self.x, self.y = x, y
        self.source_df = 'external'
        self.df_original = self.df.copy()
        self.update_after_df()

    def update_after_df(self):
        self.validate_df()
        self.ax_x.label, self.ax_y.label = self.x, self.y
        self.tt_point_loc.labels = [self.x, self.y]

    def validate_df(self):
        self.valid, msg = self.validate_df_runtime()
        error = MSG_ERROR.replace('MESSAGE', msg)
        self.ht_error.value = error if not self.valid else ''

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            msg = 'Not enough data points.'
            msg += ' At least 2 are required. Place some points.'
            return False, msg
        return True, ''

    def reset_reg(self):
        if not self.valid:
            return
        hiddens = [self.sl_h1.value, self.sl_h2.value, self.sl_h3.value]
        hiddens = [h for h in hiddens if h > 0]
        self.reg = MLPRegressor(
            self.df,
            hiddens=hiddens,
            alpha=self.sl_alpha.value,
            learn=self.sl_learn.value,
            max_iter=self.sl_max_iter.value
        )

    def update_sr_points(self):
        with self.sr_points.hold_sync():
            self.sr_points.x = self.df.x.astype(float)
            self.sr_points.y = self.df.y.astype(float)

    def update_axes(self, force=False):
        if not self.valid:
            return
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits(*limits)
        self.set_limits(*limits)

    def get_data_limits(self):
        x_min, x_max = self.df.x.min(), self.df.x.max()
        y_min, y_max = self.df.y.min(), self.df.y.max()
        return x_min, x_max, y_min, y_max

    def ck_bt_reset(self, bt_reset):
        if self.source_df == 'internal':
            self.init_df(None, '', '')
        elif self.source_df == 'external':
            self.df = self.df_original.copy()
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_mlp_regression()

    def dr_sr_points(self, sr_points, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_points = False
        if event.get('event') == 'drag':
            self.dragging_sr_points = True
        if event.get('event') == 'drag_end':
            if self.dragging_sr_points:
                point = event['point']['x'], event['point']['y']
                self.df.loc[self.df.index[event['index']], ['x', 'y']] = point
                self.update_after_df()
                self.reset_reg()
                self.update_axes()
                self.project_mlp_regression()
            else:
                index = event['index']
                self.df = self.df.drop(self.df.index[index])
                self.update_after_df()
                self.reset_reg()
                self.set_animations(False, action='store')
                self.update_sr_points()
                self.set_animations(True, action='restore')
                self.update_axes()
                self.project_mlp_regression()

    def ck_sr_points_background(self, sr_points, event):
        sr = self.sr_points
        new_point = sr.x[-1], sr.y[-1]
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        self.reset_reg()
        self.update_sr_points()
        self.update_axes()
        self.project_mlp_regression()

    def set_info(self, switch):
        for mark in self.fig.marks:
            mark.display_legend = switch
        self.sr_points.tooltip = self.tt_point_loc if switch else None

    def project_mlp_regression(self):
        if not self.valid:
            return
        x_min, x_max = self.df.x.min(), self.df.x.max()
        x = np.linspace(x_min, x_max, 300)
        y = self.reg.predict(pd.DataFrame({'x': x}))
        with self.mlp_regression.hold_sync():
            self.mlp_regression.x, self.mlp_regression.y = x, y


class MLPClassifier:
    @ignore_warnings(category=ConvergenceWarning)
    def __init__(
            self,
            df,
            hiddens=(10,),
            alpha=0.01,
            learn=0.01,
            max_iter=50,
            ):
        self.clf = neural_network.MLPClassifier(
            hidden_layer_sizes=hiddens,
            alpha=alpha,
            learning_rate_init=learn,
            max_iter=max_iter,
            activation="relu",
            solver="sgd",
            tol=1e-9,
            )
        self.clf.fit(df[['f1', 'f2']], df.c)
        self.accuracy = self.clf.score(df[['f1', 'f2']], df.c)

    @ignore_warnings(category=ConvergenceWarning)
    def fit(self, df):
        self.clf.fit(df[['f1', 'f2']], df.c)
        self.accuracy = self.clf.score(df[['f1', 'f2']], df.c)

    def predict_proba(self, df):
        preds = self.clf.predict_proba(df[['f1', 'f2']])
        return preds

    def set_params(
            self,
            hiddens=None,
            alpha=None,
            learn=None,
            max_iter=None,
            ):
        if hiddens is not None:
            self.clf.set_params(hidden_layer_sizes=hiddens)
        if alpha is not None:
            self.clf.set_params(alpha=alpha)
        if learn is not None:
            self.clf.set_params(learning_rate_init=learn)
        if max_iter is not None:
            self.clf.set_params(max_iter=max_iter)


class ValetudoMLPClassifier(ValetudoBase):
    """Interactive application for neural network classification.

    Study the basics of neural network classification.
    You can add, move, or delete points interactively to recalculate the
    classification.
    You can change the learning rate and the number of hidden layers,
    regularization coefficient, and the maximum epochs in training.

    Parameters
    ----------
    df: pandas.DataFrame, optional
        A pandas dataframe containing the data for the interactive
        application. If this argument is omitted, a random set of points
        is generated instead. The columns to be utilized should be provided
        in the appropriate arguments. Rows with not-a-numbers will be removed.
    x, y: str, optional
        If a dataframe is provided via `df`, `x` and `y` are the column
        names of the features that would be used in the interactive
        application. The data in these columns has to be numeric.

    Raises
    ------
    TypeError
        If the dataframe or column names are of invalid type.
    ValueError
        If the features data is not numeric, or the dataframe is of
        insufficient length.
    """

    def __init__(
            self, df: pd.DataFrame = None,
            f1: str = '', f2: str = '', c: str = ''
            ):
        super().__init__()
        self.create_app_controls()
        self.add_marks()
        self.bind_app_signals()
        self.init_df(df, f1, f2, c)
        self.reset_clf()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_mlp_classifier()

    def create_app_controls(self):
        self.ht_title = ipw.HTML('<h1>Neural Network classifier</h1>')
        self.bt_reset = ipw.Button(description='Reset', button_style='danger')
        self.ch_show_heat_map = ipw.Checkbox(
            value=True, description='Show Heat Map:')
        self.sl_new_point_class = ipw.IntSlider(
            value=0, min=0, max=1, description='New Point Class:',
            style={'description_width': 'initial'})
        self.ht_error = ipw.HTML()
        self.architecture_title = ipw.HTML(
            '<h3>Number of neurons in hidden layers</h3>')
        self.sl_h1 = ipw.IntSlider(
            value=5, min=1, max=10, description='HL 1:',
            style={'description_width': 'initial'})
        self.sl_h2 = ipw.IntSlider(
            value=0, min=0, max=10, description='HL 2:',
            style={'description_width': 'initial'})
        self.sl_h3 = ipw.IntSlider(
            value=0, min=0, max=10, description='HL 3:',
            style={'description_width': 'initial'})
        self.training_title = ipw.HTML("<h3>Training hyper-parameters</h3>")
        self.sl_max_iter = ipw.IntSlider(
            value=100, min=1, step=100, max=5000, description='Max iterations:',
            style={'description_width': 'initial'})
        self.sl_learn = ipw.FloatLogSlider(
            value=-2, min=-3, max=0.1, description='Learning rate:',
            style={'description_width': 'initial'})
        self.sl_alpha = ipw.FloatLogSlider(
            value=-2, min=-5, max=1, description='Regularization:',
            style={'description_width': 'initial'})
        self.accuracy_html = ipw.HTML("<h3>Classification accuracy: 0%</h3>")
        self.app_controls = [
            self.bt_reset, self.ch_show_heat_map, self.sl_new_point_class,
            self.ht_error, self.architecture_title, self.sl_h1, self.sl_h2,
            self.sl_h3, self.training_title, self.sl_max_iter, self.sl_learn,
            self.sl_alpha, self.accuracy_html
            ]

    def add_marks(self):
        self.mlp_classifier = bq.HeatMap(
            color=np.zeros((2, 2)),
            x=None,
            y=None,
            scales={
                "x": bq.LinearScale(allow_padding=False),
                "y": bq.LinearScale(allow_padding=False),
                "color": bq.ColorScale(
                    colors=['#afd7ff', '#ffffbf'],
                    )
                },
            alpha=0.1,
            labels='NN classification',
            )
        self.sr_points = bq.Scatter(
            x=[],
            y=[],
            enable_move=True,
            scales=self.scs_x_y_c,
            tooltip=self.tt_point_loc,
            stroke="#404040",
            stroke_width=2,
            labels='Points',
            interactions={'click': 'add', 'hover': 'tooltip'}
            )
        self.fig.marks = [self.mlp_classifier, self.sr_points]
        self.sc_c.domain = list(range(2))
        self.set_info(True)

    def bind_app_signals(self):
        self.bt_reset.on_click(self.ck_bt_reset)
        self.ch_show_heat_map.observe(self.cg_ch_show_heat_map, 'value')
        self.sl_new_point_class.observe(self.cg_sl_new_point_class, 'value')
        self.sr_points.on_drag_start(self.dr_sr_points)
        self.sr_points.on_drag(self.dr_sr_points)
        self.sr_points.on_drag_end(self.dr_sr_points)
        self.sr_points.on_background_click(self.ck_sr_points_background)
        # slider controls
        self.sl_h1.observe(self.cg_sl_h, 'value')
        self.sl_h2.observe(self.cg_sl_h, 'value')
        self.sl_h3.observe(self.cg_sl_h, 'value')
        self.sl_max_iter.observe(self.cg_sl_max_iter, 'value')
        self.sl_learn.observe(self.cg_sl_learn, 'value')
        self.sl_alpha.observe(self.cg_sl_alpha, 'value')
        for wd in [self.sl_new_point_class]:
            ipw.dlink(
                (wd, 'value'), (wd.style, 'handle_color'),
                transform=self.get_color_for_n)

    def cg_ch_show_heat_map(self, change):
        self.mlp_classifier.visible = change['new']

    def cg_sl_new_point_class(self, change):
        # fix for bqplot not coloring points immediately
        color = self.get_color_for_n(change['new'])
        self.sl_new_point_class.style.handle_color = color
        self.sr_points.color = self.sr_points.color[:-1]
        self.preplace_color_for_new_point()

    def preplace_color_for_new_point(self):
        # fix for bqplot not coloring points immediately
        color_value = self.sl_new_point_class.value
        self.sr_points.color = np.append(self.sr_points.color, color_value)

    def cg_sl_h(self, change):
        hiddens = [self.sl_h1.value, self.sl_h2.value, self.sl_h3.value]
        hiddens = [h for h in hiddens if h > 0]
        self.clf.set_params(hiddens=hiddens)
        self.clf.fit(self.df)
        self.update_accuracy(self.clf.accuracy)
        self.project_mlp_classifier()

    def update_accuracy(self, accuracy):
        self.accuracy_html.value = (
            f"<h3>Classification accuracy: {accuracy*100:.2f}%</h3>")

    def cg_sl_max_iter(self, change):
        self.clf.set_params(max_iter=change["new"])
        self.clf.fit(self.df)
        self.update_accuracy(self.clf.accuracy)
        self.project_mlp_classifier()

    def cg_sl_learn(self, change):
        self.clf.set_params(learn=change["new"])
        self.clf.fit(self.df)
        self.update_accuracy(self.clf.accuracy)
        self.project_mlp_classifier()

    def cg_sl_alpha(self, change):
        self.clf.set_params(alpha=change["new"])
        self.clf.fit(self.df)
        self.update_accuracy(self.clf.accuracy)
        self.project_mlp_classifier()

    def init_df(self, df, f1, f2, c):
        column_names = ['f1', 'f2', 'c']
        if df is None:
            self.f1, self.f2, self.c = column_names
            X, y = make_moons(n_samples=100, noise=0.2)
            self.df = pd.DataFrame({"f1": X[:, 0], "f2": X[:, 1], "c": y})
            self.source_df = 'internal'
            self.df_original = self.df.copy()
            self.update_after_df()
            return
        self.check_arg_df(df)
        [self.check_arg_col_numeric(df, col) for col in [f1, f2]]
        self.check_arg_col(df, c)
        self.df = df[[f1, f2, c]].copy().dropna()
        self.check_arg_df_min_rows(self.df, 1)
        self.df.columns = column_names
        self.f1, self.f2, self.c = f1, f2, c
        self.le = preprocessing.LabelEncoder()
        self.df.c = self.le.fit_transform(self.df.c)
        self.update_ht_classes()
        self.source_df = 'external'
        self.df_original = self.df.copy()
        self.update_after_df()

    def update_ht_classes(self):
        one_class = '<span style="color:{}">⬤</span> {}<br>'
        classes_text = '<h2>Classes</h2>'
        for index, class_name in enumerate(self.le.classes_):
            color = self.get_color_for_n(index)
            classes_text += one_class.format(color, class_name)
        self.ht_classes.value = classes_text

    def update_after_df(self):
        self.validate_df()
        self.ax_x.label, self.ax_y.label = self.f1, self.f2
        self.tt_point_loc.labels = [self.f1, self.f2]
        n_classes_max = max(self.df.c, default=-1)
        self.sl_new_point_class.max = n_classes_max
        self.sc_c.domain = list(range(n_classes_max + 1))

    def validate_df(self):
        self.valid, msg = self.validate_df_runtime()
        error = MSG_ERROR.replace('MESSAGE', msg)
        self.ht_error.value = error if not self.valid else ''

    def validate_df_runtime(self):
        if self.df.shape[0] < 2:
            msg = 'Not enough data points.'
            msg += ' At least 2 are required. Place some points.'
            return False, msg
        if len(self.df.c.unique()) > 2:
            msg = 'Too many classes. Only 2 classes supported!'
            return False, msg
        return True, ''

    def reset_clf(self):
        hiddens = [self.sl_h1.value, self.sl_h2.value, self.sl_h3.value]
        hiddens = [h for h in hiddens if h > 0]
        self.clf = MLPClassifier(
            self.df,
            hiddens=hiddens,
            alpha=self.sl_alpha.value,
            learn=self.sl_learn.value,
            max_iter=self.sl_max_iter.value,
            )
        self.update_accuracy(self.clf.accuracy)

    def update_sr_points(self):
        with self.sr_points.hold_sync():
            self.sr_points.x = self.df.f1.astype(float)
            self.sr_points.y = self.df.f2.astype(float)
            self.sr_points.color = self.df.c
        self.preplace_color_for_new_point()

    def update_axes(self, force=False):
        if not self.valid:
            return
        if not self.tb_autorange.value and not force:
            return
        limits = self.get_data_limits()
        limits = self.expand_limits(*limits)
        self.set_limits(*limits)

    def get_data_limits(self):
        x_min, x_max = self.df.f1.min(), self.df.f1.max()
        y_min, y_max = self.df.f2.min(), self.df.f2.max()
        return x_min, x_max, y_min, y_max

    def ck_bt_reset(self, bt_reset):
        if self.source_df == 'internal':
            self.init_df(None, '', '', '')
        elif self.source_df == 'external':
            self.df = self.df_original.copy()
        self.update_after_df()
        self.reset_clf()
        self.update_sr_points()
        self.update_axes(force=True)
        self.project_mlp_classifier()

    def dr_sr_points(self, sr_points, event):
        if event.get('event') == 'drag_start':
            self.dragging_sr_points = False
        if event.get('event') == 'drag':
            self.dragging_sr_points = True
        if event.get('event') == 'drag_end':
            if self.dragging_sr_points:
                point = event['point']['x'], event['point']['y']
                index = self.df.index[event['index']]
                self.df.loc[index, ['f1', 'f2']] = point
                self.reset_clf()
                self.update_axes()
                self.project_mlp_classifier()
            else:
                index = event['index']
                self.df = self.df.drop(self.df.index[index])
                self.update_after_df()
                self.reset_clf()
                self.set_animations(False, action='store')
                self.update_sr_points()
                self.set_animations(True, action='restore')
                self.update_axes()
                self.project_mlp_classifier()

    def ck_sr_points_background(self, sr_points, event):
        sr = self.sr_points
        new_point = sr.x[-1], sr.y[-1], sr.color[-1]
        its_a_new_class = self.sr_points.color[-1] not in self.df.c.values
        df_new_point = pd.DataFrame([new_point], columns=self.df.columns)
        self.df = pd.concat([self.df, df_new_point], ignore_index=True)
        self.update_after_df()
        if its_a_new_class:
            self.reset_clf()
        self.update_sr_points()
        self.update_axes()
        self.project_mlp_classifier()

    def set_info(self, switch):
        for mark in self.fig.marks:
            mark.display_legend = switch
        self.sr_points.tooltip = self.tt_point_loc if switch else None

    def project_mlp_classifier(self):
        if not self.valid:
            return
        limits = self.get_data_limits()
        x_min, x_max, y_min, y_max = self.expand_limits(*limits)
        h = 100
        x, y = np.linspace(x_min, x_max, h), np.linspace(y_min, y_max, h)
        xx0, xx1 = np.meshgrid(x, y)
        feats = np.column_stack([xx0.ravel(), xx1.ravel()])
        df = pd.DataFrame({"f1": feats[:, 0], "f2": feats[:, 1]})
        z = self.clf.predict_proba(df)[:, 1]
        z = z.reshape(xx0.shape)
        with self.mlp_classifier.hold_sync():
            self.mlp_classifier.color = z
            self.mlp_classifier.x = x
            self.mlp_classifier.y = y
